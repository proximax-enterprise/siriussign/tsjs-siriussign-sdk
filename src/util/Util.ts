import { QRCode, ErrorCorrectLevel, QRNumber, QRAlphaNum, QR8BitByte, QRKanji } from 'qrcode-generator-ts/js';

export class Util {

    /**
     * Delay in ms (ms)
     * @param ms
     */
    public static sleep(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    /**
     * Asynchronous forEach
     * @param array 
     * @param callback 
     */
    public static async asyncForEach(array: any[], callback: any, breakFn: any = () => false) {
        for (let index = 0; index < array.length; index++) {
            if (breakFn()) break;
            await callback(array[index], index, array);
        }
    }

    /**
     * Generate QR code from string data
     * @param data
     * @param numberLevel
     */
    public static getQrCode(data: string, numberLevel: number) {
        let qrCode = new QRCode;
        qrCode.addData(data);
        qrCode.setTypeNumber(numberLevel);
        qrCode.setErrorCorrectLevel(ErrorCorrectLevel.L);
        qrCode.make();
        let url = qrCode.toDataURL();
        return url;
    }
}