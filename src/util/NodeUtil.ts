import { BlockHttp, NodeHttp } from "tsjs-xpx-chain-sdk";

export class NodeUtil {
    public static split(node: string) {
        const protocol = node.split('://')[0];
        const domain = node.split('//')[1].split(':')[0];
        const portString = node.split('//')[1].split(':')[1];
        const port: number | undefined = (portString) ? parseInt(portString, 10) : undefined;
        return { domain: domain, protocol: protocol, port: port };
    }

    public static getWebsocketAddress(apiNode: string) {
        return apiNode.replace('https', 'wss').replace('http', 'ws');
    }

    public static async fetchNetworkGenerationHash(apiNode: string) {
        const blockHttp = new BlockHttp(apiNode);
        const genesisBlock = await blockHttp.getBlockByHeight(1).toPromise();
        return genesisBlock.generationHash;
    }

    public static async fetchNetworkType(apiNode: string) {
        const nodeHttp = new NodeHttp(apiNode);
        const nodeInfo = await nodeHttp.getNodeInfo().toPromise();
        return nodeInfo.networkIdentifier;
    }
}