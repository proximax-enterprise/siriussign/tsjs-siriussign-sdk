import * as JSJoda from 'js-joda';

export class ConverterUtil {
    /**
     * Convert base64 string to hex string
     * @param base64 
     */
    public static base64ToHex(base64: string): string {
        // let raw = Buffer.from(base64, 'base64').toString(); // atob(base64);
        // let hex = '';
        // for (let i = 0; i < raw.length; i++) {
        //     let _hex = raw.charCodeAt(i).toString(16)
        //     hex += (_hex.length == 2 ? _hex : '0' + _hex);
        // }
        // return hex;
        return Buffer.from(base64, 'base64').toString('hex');
    }

    /**
     * Convert hex string to base64 string
     * @param hex 
     */
    public static hexToBase64(hex: string) {
        // return btoa(String.fromCharCode.apply(null,
        //     hex.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" "))
        // );
        return Buffer.from(hex, 'hex').toString('base64');
    }

    /**
     * Convert bytes array to base64
     */
    public static uint8ArrayToBase64(bytes: Uint8Array): string {
        const base64abc = (() => {
            let abc = [],
                A = "A".charCodeAt(0),
                a = "a".charCodeAt(0),
                n = "0".charCodeAt(0);
            for (let i = 0; i < 26; ++i) {
                abc.push(String.fromCharCode(A + i));
            }
            for (let i = 0; i < 26; ++i) {
                abc.push(String.fromCharCode(a + i));
            }
            for (let i = 0; i < 10; ++i) {
                abc.push(String.fromCharCode(n + i));
            }
            abc.push("+");
            abc.push("/");
            return abc
        })();

        let result = '', i, l = bytes.length;
        for (i = 2; i < l; i += 3) {
            result += base64abc[bytes[i - 2] >> 2];
            result += base64abc[((bytes[i - 2] & 0x03) << 4) | (bytes[i - 1] >> 4)];
            result += base64abc[((bytes[i - 1] & 0x0F) << 2) | (bytes[i] >> 6)];
            result += base64abc[bytes[i] & 0x3F];
        }
        if (i === l + 1) { // 1 octet missing
            result += base64abc[bytes[i - 2] >> 2];
            result += base64abc[(bytes[i - 2] & 0x03) << 4];
            result += "==";
        }
        if (i === l) { // 2 octets missing
            result += base64abc[bytes[i - 2] >> 2];
            result += base64abc[((bytes[i - 2] & 0x03) << 4) | (bytes[i - 1] >> 4)];
            result += base64abc[(bytes[i - 1] & 0x0F) << 2];
            result += "=";
        }
        return result;
    }

    /**
     * Convert data URI to Base64 Unit8Array
     * @param dataURI 
     */
    public static dataURIToUint8Array(dataURI: string): Uint8Array {
        const BASE64_MARKER = ';base64,';
        const base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        const base64 = dataURI.substring(base64Index);
        const raw = Buffer.from(base64, 'base64').toString(); // window.atob(base64);
        const rawLength = raw.length;
        const array = new Uint8Array(new ArrayBuffer(rawLength));

        for (let i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array;
    }

    /**
     * Convert base64 data string to base64 dataURI
     * @param base64
     * @param fileType
     */
    public static base64ToDataURI(base64: string, fileType: string) {
        return 'data:' + fileType + ';base64,' + base64;
    }

    /**
     * Get Date object from localTime
     * @param localTime
     * @param adjHourAmount
     * @param altLocalTime
     */
    public static localTimeToDate(localTime: JSJoda.LocalDateTime, adjHourAmount?: number, altUTC?: number) {
        const adjLocalTime = adjHourAmount ? localTime.plusHours(adjHourAmount) : localTime;
        let date = JSJoda.convert(adjLocalTime).toDate();

        // To prevent negative time
        const now = Date.now();
        let millis = now - date.getTime();
        if (millis < 0 && altUTC) date = new Date(altUTC);
        return date;
    }
}