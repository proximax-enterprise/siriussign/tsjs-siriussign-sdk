import { Listener, Address, Transaction, TransactionHttp, SignedTransaction, TransactionAnnounceResponse } from "tsjs-xpx-chain-sdk";
import { NodeUtil } from "../util/NodeUtil";

export type AnnouceTxFn = () => Promise<TransactionAnnounceResponse>;

export class TransactionListener {
    listener: Listener;
    apiNode: string;
    ws: string;
    websocketInject: any;

    constructor(apiNode: string, websocketInject?: any) {
        this.apiNode = apiNode;
        this.ws = NodeUtil.getWebsocketAddress(apiNode);
        this.websocketInject = websocketInject;
        this.listener = new Listener(this.ws, this.websocketInject);
    }

    /**
     * Open websocket to listen events
     */
    open() {
        this.listener = new Listener(this.ws, this.websocketInject);
        return this.listener.open();
    }

    /**
     * Check if the connection is still open
     */
    check() {
        if (!this.listener) return false;
        const listenerAsAny = this.listener as any;
        if (!listenerAsAny.webSocket) return false;
        return listenerAsAny.webSocket.readyState === 1;
    }

    /**
     * Return current websocket state
     */
    getState() {
        const listenerAsAny = this.listener as any;
        return listenerAsAny.webSocket.readyState;
    }

    /**
     * Check if the connection is still open, if not then reopen
     */
    async checkAndOpenListener() {
        if (!this.check()) await this.open();
    }

    /**
     * Close websocket
     */
    close() {
        this.listener.close();
        console.log('connection closed');
    }

    /**
     * Announce a transaction to network
     * @param signedTx
     */
    announceTransaction(signedTx: SignedTransaction) {
        const transactionHttp = new TransactionHttp(this.apiNode);
        return transactionHttp
            .announce(signedTx)
            .toPromise();
    }

    /**
     * Announce and wait for transaction confirmed or failed
     * @param address
     * @param signedTxHash
     * @param fnAnnounce
     * @param timeoutPolling
     */
    async waitForTransactionConfirmed(address: Address, signedTxHash: string, fnAnnounce: AnnouceTxFn, timeoutPolling: number = 30000) {
        await this.checkAndOpenListener();
        return new Promise<Transaction>((resolve, reject) => {
            const statusSub = this.listener.status(address)
                .subscribe(
                    status => {
                        clearTimeout(timeoutListener);
                        reject(status);
                        confirmSub.unsubscribe();
                        statusSub.unsubscribe();
                        this.close();
                    },
                    error => { reject(error.status); },
                    () => { console.log('[Listener] Status Listener Done'); }
                );

            const confirmSub = this.listener.confirmed(address)
                .subscribe(
                    successTx => {
                        if (successTx && successTx.transactionInfo && successTx.transactionInfo.hash === signedTxHash) {
                            clearTimeout(timeoutListener);
                            resolve(successTx);
                            confirmSub.unsubscribe();
                            statusSub.unsubscribe();
                            this.close();
                        }
                    },
                    error => { reject(error.status); },
                    () => { console.log('[Listener] Confirmed Listener Done.'); }
                );
            fnAnnounce().catch(err => { reject('Cannot anounce the transaction'); });

            const timeoutListener = setTimeout(async () => {
                confirmSub.unsubscribe();
                statusSub.unsubscribe();
                this.close();
                console.log('Listener failed. Start polling');
                const transactionHttp = new TransactionHttp(this.apiNode);
                const status = await transactionHttp.getTransactionStatus(signedTxHash).toPromise().catch(err => {
                    reject('Cannot get the transaction status');
                    return null;
                });

                if (!status) return;

                if (status.group == 'unconfirmed') {
                    const statusPolling = setInterval(async () => {
                        const statusReget = await transactionHttp.getTransactionStatus(signedTxHash).toPromise();
                        if (statusReget.group == 'confirmed') {
                            const tx = await transactionHttp.getTransaction(signedTxHash).toPromise();
                            resolve(tx);
                            clearInterval(statusPolling);
                        }
                        if (statusReget.group == 'failed') {
                            reject(statusReget);
                            clearInterval(statusPolling);
                        }
                    }, 500);
                }

                if (status.group == 'confirmed') {
                    const tx = await transactionHttp.getTransaction(signedTxHash).toPromise();
                    resolve(tx);
                }

                if (status.group == 'failed') {
                    reject(status);
                }
            }, timeoutPolling);
        });
    }
}