import {
    NetworkType,
    PublicAccount,
    AggregateTransaction,
    Transaction,
    TransferTransaction,
    Address,
    TransactionType
} from "tsjs-xpx-chain-sdk";
import { DocumentInfoService } from "./DocumentInfoService";
import { SiriusSignDocumentInfo, ConverterUtil, SiriusSignDocumentSigningMessage, SiriusSignMessageType } from "..";
import { Util } from "../util/Util";

export class DocumentFetch extends DocumentInfoService {

    isFetching: boolean;

    constructor(
        appID: string,
        apiNode: string,
        networkType: NetworkType,
        queryPageSize: number = 10
    ) {
        super(apiNode, networkType, queryPageSize);
        this.isFetching = false;
        this.setStrictFilter(appID);
    }

    /**
     * Fetch document signing notifications
     */
    async documentSigningNotifications(userAccount: PublicAccount, lastHash: string) {
        const lastId = await this.fetchTransactionId(lastHash);
        // Fetch all incomming transaction of user account
        const aggCompleteTxsFromConfirmedTxs = await this.fetchAggregateCommpleteTransactions(userAccount, lastId);
        const aggCompleteTxs = aggCompleteTxsFromConfirmedTxs.txs;
        const newLastHash = aggCompleteTxsFromConfirmedTxs.lastHash;
        // Filter document signing notificaion transaction - Aggregate Complete Tx with NOTIFY TransferTx inner
        const documentSigningNotifications = this.filterDocumentSigningNotifications(aggCompleteTxs);
        return { txs: documentSigningNotifications, lastHash: newLastHash };
    }

    /**
     * Fetch document signing transactions of a user by user's account
     * @param userAccount
     */
    async documentSigningTransactionsByUserAccount(userAccount: PublicAccount, lastHash: string) {
        const lastId = await this.fetchTransactionId(lastHash);
        const outgoingTxs = await this.fetchOutgoingTransactions(userAccount, lastId);
        const docSigningTxs: TransferTransaction[] = this.filterDocumentSigningTransations(outgoingTxs);
        const newLastHash = outgoingTxs[outgoingTxs.length - 1]?.transactionInfo?.hash ?? null;
        return { txs: docSigningTxs, lastHash: newLastHash };
    }

    /**
     * Fetch a Sirius Sign document by document account publickey
     */
    async byDocumentAccount(documentPublicKey: string, userAccount?: PublicAccount) {
        const docPublicAcc = PublicAccount.createFromPublicKey(documentPublicKey, this.networkType);

        // Fetch all outgoing transactions of document account and filter NOTI transaction to get signers and verifiers
        const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
        const docAggTxs = docOutgoingTxs
            .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
            .map(aggTx => <AggregateTransaction>aggTx);
        const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);
        const notiAggTx = notiAggTxs[0];
        let { cosigners, verifiers } = await this.getCosignerVerifiersFromNotiAggTx(notiAggTx, userAccount);

        // Fetch all incomming transactions of document account and filter SIGN and VERIFY transactions to get signatures
        const docIncomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);
        if (docIncomingTxs.length == 0) return null;
        const signedInfo = this.getSignedSignerInfo(docIncomingTxs, userAccount);

        const owner = signedInfo.owner;
        const ownerSigningTx = signedInfo.ownerSigningTx;
        const ownerSigningMsg = signedInfo.ownerSigningMsg;
        const cosigneds = signedInfo.cosigners;
        const verifieds = signedInfo.verifiers;
        this.updateSigners(cosigners, cosigneds);
        this.updateSigners(verifiers, verifieds);
        const uploadDate = ConverterUtil.localTimeToDate(ownerSigningTx.deadline.value, -2);

        const doc = SiriusSignDocumentInfo.create(
            docPublicAcc.address.plain(),
            ownerSigningMsg.body.fileName,
            ownerSigningMsg.body.fileType,
            ownerSigningMsg.body.fileHash,
            uploadDate,
            owner,
            cosigners,
            docPublicAcc,
            ownerSigningMsg.body.uploadTxHash,
            ownerSigningMsg.body.encryptType,
            verifiers,
        );
        return doc;
    }

    /**
     * Fetch a Sirius Sign document by signing transction hash
     * @param hash
     */
    async bySigningTxHash(hash: string) {
        const signTx: Transaction = await this.fetchTransaction(hash).catch(err => { return null; });
        if (!signTx) return null;
        if (signTx.type != TransactionType.TRANSFER) return null;
        const tx = <TransferTransaction>signTx;
        const info = <SiriusSignDocumentSigningMessage>this.parseSiriusSignMessage(tx.message.payload);
        const docPublicAcc = await this.fetchPublicAccount(<Address>tx.recipient);

        // Fetch all outgoing transactions of document account and filter NOTI transaction to get signers and verifiers
        const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
        const docAggTxs = docOutgoingTxs
            .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
            .map(aggTx => <AggregateTransaction>aggTx);
        const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);
        const notiAggTx = notiAggTxs[0];
        let { cosigners, verifiers } = await this.getCosignerVerifiersFromNotiAggTx(notiAggTx);

        // Fetch all incomming transactions of document account and filter SIGN and VERIFY transactions to get signatures
        const docIncomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);
        const signedInfo = this.getSignedSignerInfo(docIncomingTxs);

        const owner = signedInfo.owner;
        const ownerSigningTx = signedInfo.ownerSigningTx;
        const ownerSigningMsg = signedInfo.ownerSigningMsg;
        const cosigneds = signedInfo.cosigners;
        const verifieds = signedInfo.verifiers;
        this.updateSigners(cosigners, cosigneds);
        this.updateSigners(verifiers, verifieds);
        const uploadDate = ConverterUtil.localTimeToDate(ownerSigningTx.deadline.value, -2);

        const doc = SiriusSignDocumentInfo.create(
            docPublicAcc.address.plain(),
            ownerSigningMsg.body.fileName,
            ownerSigningMsg.body.fileType,
            ownerSigningMsg.body.fileHash,
            uploadDate,
            owner,
            cosigners,
            docPublicAcc,
            ownerSigningMsg.body.uploadTxHash,
            ownerSigningMsg.body.encryptType,
            verifiers,
        );
        return doc;
    }

    /**
     * Fetch need-sign and need-verify documents info from a page of transactions
     * @param userAccount PublicAccount
     * @param lastHash hash of the latest transaction of a page
     */
    async needSignVerify(userAccount: PublicAccount, lastHash: string) {
        let needSign: SiriusSignDocumentInfo[] = [];
        let needVerify: SiriusSignDocumentInfo[] = [];
        const docSigningNotiTxsFromConfirmedTxs = await this.documentSigningNotifications(userAccount, lastHash).catch(err => {
            console.log(err);
            return { txs: [], lastHash: null };
        });
        const notiAggTxs: AggregateTransaction[] = docSigningNotiTxsFromConfirmedTxs.txs;
        const newLastHash = docSigningNotiTxsFromConfirmedTxs.lastHash;
        const fetchNeedSign = async () => Util.asyncForEach(notiAggTxs, async (notiAggTx: AggregateTransaction, index: number) => {
            //Fetch all incomming transactions of document account
            const docPublicAcc = await this.fetchPublicAccount(notiAggTx.signer.address);
            const incomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);

            // Filter SIGN transactions to get signatures
            const signedInfo = this.getSignedSignerInfo(incomingTxs, userAccount);

            const owner = signedInfo.owner;
            const ownerSigningTx = signedInfo.ownerSigningTx;
            const ownerSigningMsg = signedInfo.ownerSigningMsg;
            const cosigneds = signedInfo.cosigners;
            const verifieds = signedInfo.verifiers;
            const uploadDate = ConverterUtil.localTimeToDate(ownerSigningTx.deadline.value, -2);
            const isSigned = [owner, ...cosigneds, ...verifieds].map(signer => signer.address).includes(userAccount.address.plain());

            if (!isSigned) {
                // Check cosigners
                let { cosigners, verifiers } = await this.getCosignerVerifiersFromNotiAggTx(notiAggTx, userAccount);
                this.updateSigners(cosigners, cosigneds);
                this.updateSigners(verifiers, verifieds);

                const doc = SiriusSignDocumentInfo.create(
                    notiAggTx.signer.address.plain(),
                    ownerSigningMsg.body.fileName,
                    ownerSigningMsg.body.fileType,
                    ownerSigningMsg.body.fileHash,
                    uploadDate,
                    owner,
                    cosigners,
                    notiAggTx.signer,
                    ownerSigningMsg.body.uploadTxHash,
                    ownerSigningMsg.body.encryptType,
                    verifiers
                );

                const isNeedSign = cosigners.map(cosigner => cosigner.address).includes(userAccount.address.plain());
                const isNeedVerify = verifiers.map(verifier => verifier.address).includes(userAccount.address.plain());
                if (isNeedSign) needSign.push(doc);
                if (isNeedVerify) needVerify.push(doc);
            }

            await Util.sleep(200);
            if (index > 0 && (index % 2) == 0) {
                await Util.sleep(200);
            }
        });
        await fetchNeedSign().catch(err => {
            console.log(err);
        });

        return { needSign: needSign, needVerify: needVerify, nextHash: newLastHash };
    }

    /**
     * Fetch completely signed, partially signed, verified, verifying documents info from a page of transactions
     * @param userAccount PublicAccount
     * @param lastHash hash of the latest transaction of a page
     */
    async completedAndWaiting(userAccount: PublicAccount, lastHash: string) {
        let waiting: SiriusSignDocumentInfo[] = [];
        let completed: SiriusSignDocumentInfo[] = [];
        let verifying: SiriusSignDocumentInfo[] = [];
        let verified: SiriusSignDocumentInfo[] = [];
        const docSignTxsFromOutgoingTxs = await this.documentSigningTransactionsByUserAccount(userAccount, lastHash);
        const signTxs = docSignTxsFromOutgoingTxs.txs;
        const newLastHash = docSignTxsFromOutgoingTxs.lastHash;
        const fetchDocuments = async () => {
            await Util.asyncForEach(signTxs, async (tx: TransferTransaction, index: number) => {
                const info = <SiriusSignDocumentSigningMessage>this.parseSiriusSignMessage((<TransferTransaction>tx).message.payload);
                const docPublicAcc = await this.fetchPublicAccount(<Address>tx.recipient);

                // Fetch noti agg transaction
                const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
                const docAggTxs = docOutgoingTxs
                    .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
                    .map(aggTx => <AggregateTransaction>aggTx);
                const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);

                // Fetch cosigners and verifiers
                const notiAggTx = notiAggTxs[0];
                let { cosigners, verifiers } = await this.getCosignerVerifiersFromNotiAggTx(notiAggTx, userAccount);

                //Fetch all incomming transactions of document account
                const incomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);

                // Filter SIGN and VERIFY transactions to get signatures
                const signedInfo = this.getSignedSignerInfo(incomingTxs, userAccount);

                const owner = signedInfo.owner;
                const ownerSigningTx = signedInfo.ownerSigningTx;
                const ownerSigningMsg = signedInfo.ownerSigningMsg;
                const cosigneds = signedInfo.cosigners;
                const verifieds = signedInfo.verifiers;
                this.updateSigners(cosigners, cosigneds);
                this.updateSigners(verifiers, verifieds);

                const uploadDate = ConverterUtil.localTimeToDate(ownerSigningTx.deadline.value, -2);

                const doc = SiriusSignDocumentInfo.create(
                    docPublicAcc.address.plain(),
                    ownerSigningMsg.body.fileName,
                    ownerSigningMsg.body.fileType,
                    ownerSigningMsg.body.fileHash,
                    uploadDate,
                    owner,
                    cosigners,
                    docPublicAcc,
                    ownerSigningMsg.body.uploadTxHash,
                    ownerSigningMsg.body.encryptType,
                    verifiers,
                );
                const isSigner = doc.checkIsSigner(userAccount.address.plain());
                const isVerifier = doc.checkIsVerifier(userAccount.address.plain());
                if (isSigner) {
                    if (doc.checkIsCompleted()) completed.push(doc);
                    else waiting.push(doc);
                }
                if (isVerifier) {
                    if (doc.checkIsVerified()) verified.push(doc);
                    else verifying.push(doc);
                }

                await Util.sleep(200);
                if (index > 0 && (index % 2) == 0) {
                    await Util.sleep(200);
                }
            });
        }
        await fetchDocuments().catch(err => {
            console.log(err);
        });

        return { completed: completed, waiting: waiting, verified: verified, verifying: verifying, nextHash: newLastHash };
    }


    /**
     * Fetch all document signing notifications
     */
    async allDocumentSigningNotifications(userAccount: PublicAccount) {
        // Fetch all incomming transaction of user account
        let allAggCompleteTxs: AggregateTransaction[] = await this.fetchAllAggregateCommpleteTransactions(userAccount);
        // Filter document signing notificaion transaction - Aggregate Complete Tx with NOTIFY TransferTx inner
        const documentSigningNotifications = this.filterDocumentSigningNotifications(allAggCompleteTxs);
        return documentSigningNotifications;
    }

    /**
     * Fetch all document signing transactions of a document by its account
     * @param documentPublicKey
     */
    async allDocumentSigningTransactionsByDocumentAccount(documentPublicKey: string) {
        const documentPublicAccount = PublicAccount.createFromPublicKey(documentPublicKey, this.networkType);
        let allDocSigningTxs: TransferTransaction[] = [];
        await this.fetchAll(documentPublicAccount, false,
            (publicAccount: PublicAccount, lastId: string) => this.fetchIncomingTransactions(publicAccount, lastId),
            (txs: Transaction[]) => {
                txs.forEach(tx => {
                    if (tx.type == TransactionType.TRANSFER) {
                        const info = this.parseSiriusSignMessage((<TransferTransaction>tx).message.payload);
                        if (info && (this.checkStrict(info.header.appID)) &&
                            ((info.header.messageType == SiriusSignMessageType.SIGN))) {
                            allDocSigningTxs = [...allDocSigningTxs, <TransferTransaction>tx];
                        }
                    }
                });
            })
        return allDocSigningTxs;
    }

    /**
     * Fetch all document signing transactions of a user by user's account
     * @param userAccount
     */
    async allDocumentSigningTransactionsByUserAccount(userAccount: PublicAccount) {
        let allDocSigningTxs: TransferTransaction[] = [];
        await this.fetchAll(userAccount, false,
            (publicAccount: PublicAccount, lastId: string) => this.fetchOutgoingTransactions(publicAccount, lastId),
            (txs: Transaction[]) => {
                txs.forEach(tx => {
                    if (tx.type == TransactionType.TRANSFER) {
                        const info = this.parseSiriusSignMessage((<TransferTransaction>tx).message.payload);
                        if (info && (info.header.appID == this.appID) &&
                            ((info.header.messageType == SiriusSignMessageType.SIGN) || (info.header.messageType == SiriusSignMessageType.VERIFY))) {
                            allDocSigningTxs = [...allDocSigningTxs, <TransferTransaction>tx];
                        }
                    }
                });
            })
        return allDocSigningTxs;
    }

    /**
     * Fetch all document that need user to sign
     */
    async allNeedSignVerify(userAccount: PublicAccount, fnStopFetching: any = () => false) {
        if (this.isFetching) return;
        this.isFetching = true;
        let needSign: SiriusSignDocumentInfo[] = [];
        let needVerify: SiriusSignDocumentInfo[] = [];
        const notiAggTxs: AggregateTransaction[] = await this.allDocumentSigningNotifications(userAccount).catch(err => {
            console.log(err);
            this.isFetching = false;
            return [];
        });
        const fetchNeedSign = async () => Util.asyncForEach(notiAggTxs, async (notiAggTx: AggregateTransaction, index: number) => {
            //Fetch all incomming transactions of document account
            const docPublicAcc = await this.fetchPublicAccount(notiAggTx.signer.address);
            const incomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);

            // Filter SIGN transactions to get signatures
            const signedInfo = this.getSignedSignerInfo(incomingTxs, userAccount);

            const owner = signedInfo.owner;
            const ownerSigningTx = signedInfo.ownerSigningTx;
            const ownerSigningMsg = signedInfo.ownerSigningMsg;
            const cosigneds = signedInfo.cosigners;
            const verifieds = signedInfo.verifiers;
            const uploadDate = ConverterUtil.localTimeToDate(ownerSigningTx.deadline.value, -2);
            const isSigned = [owner, ...cosigneds, ...verifieds].map(signer => signer.address).includes(userAccount.address.plain());

            if (!isSigned) {
                // Check cosigners
                let { cosigners, verifiers } = await this.getCosignerVerifiersFromNotiAggTx(notiAggTx, userAccount);
                this.updateSigners(cosigners, cosigneds);
                this.updateSigners(verifiers, verifieds);

                const doc = SiriusSignDocumentInfo.create(
                    notiAggTx.signer.address.plain(),
                    ownerSigningMsg.body.fileName,
                    ownerSigningMsg.body.fileType,
                    ownerSigningMsg.body.fileHash,
                    uploadDate,
                    owner,
                    cosigners,
                    notiAggTx.signer,
                    ownerSigningMsg.body.uploadTxHash,
                    ownerSigningMsg.body.encryptType,
                    verifiers
                );

                const isNeedSign = cosigners.map(cosigner => cosigner.address).includes(userAccount.address.plain());
                const isNeedVerify = verifiers.map(verifier => verifier.address).includes(userAccount.address.plain());
                if (isNeedSign) needSign.push(doc);
                if (isNeedVerify) needVerify.push(doc);
            }

            await Util.sleep(200);
            if (index > 0 && (index % 2) == 0) {
                await Util.sleep(200);
            }
        }, () => fnStopFetching());
        await fetchNeedSign().catch(err => {
            console.log(err);
            this.isFetching = false;
        });

        if (fnStopFetching()) {
            this.isFetching = false;
            return;
        }

        this.isFetching = false;
        return { needSign, needVerify };
    }

    /**
     * Fetch all document info that user account is a co-signer but has not signed yet
     */
    async allNeedSign(userAccount: PublicAccount, fnStopFetching: any = () => false) {
        const { needSign, needVerify } = await this.allNeedSignVerify(userAccount, fnStopFetching);
        return needSign;
    }

    /**
     * Fetch all document info that user account is a verifier but has not verified yet
     */
    async allNeedVerify(userAccount: PublicAccount, fnStopFetching: any = () => false) {
        const { needSign, needVerify } = await this.allNeedSignVerify(userAccount, fnStopFetching);
        return needVerify;
    }

    /**
     * Fetch all compeleted and waiting documents
     */
    async allCompletedAndWaiting(userAccount: PublicAccount, fnStopFetching: any = () => false) {
        if (this.isFetching) return;
        this.isFetching = true;
        let waiting: SiriusSignDocumentInfo[] = [];
        let completed: SiriusSignDocumentInfo[] = [];
        let verifying: SiriusSignDocumentInfo[] = [];
        let verified: SiriusSignDocumentInfo[] = [];
        const signTxs = await this.allDocumentSigningTransactionsByUserAccount(userAccount);
        const fetchDocuments = async () => {
            await Util.asyncForEach(signTxs, async (tx: TransferTransaction, index: number) => {
                const info = <SiriusSignDocumentSigningMessage>this.parseSiriusSignMessage((<TransferTransaction>tx).message.payload);
                const docPublicAcc = await this.fetchPublicAccount(<Address>tx.recipient);

                // Fetch noti agg transaction
                const docOutgoingTxs = await this.fetchAllOutgoingTransactions(docPublicAcc);
                const docAggTxs = docOutgoingTxs
                    .filter(outTx => outTx.type == TransactionType.AGGREGATE_COMPLETE)
                    .map(aggTx => <AggregateTransaction>aggTx);
                const notiAggTxs = this.filterDocumentSigningNotifications(docAggTxs);

                // Fetch cosigners and verifiers
                const notiAggTx = notiAggTxs[0];
                let { cosigners, verifiers } = await this.getCosignerVerifiersFromNotiAggTx(notiAggTx, userAccount);

                //Fetch all incomming transactions of document account
                const incomingTxs: Transaction[] = await this.fetchAllIncomingTransactions(docPublicAcc);

                // Filter SIGN and VERIFY transactions to get signatures
                const signedInfo = this.getSignedSignerInfo(incomingTxs, userAccount);

                const owner = signedInfo.owner;
                const ownerSigningTx = signedInfo.ownerSigningTx;
                const ownerSigningMsg = signedInfo.ownerSigningMsg;
                const cosigneds = signedInfo.cosigners;
                const verifieds = signedInfo.verifiers;
                this.updateSigners(cosigners, cosigneds);
                this.updateSigners(verifiers, verifieds);

                const uploadDate = ConverterUtil.localTimeToDate(ownerSigningTx.deadline.value, -2);

                const doc = SiriusSignDocumentInfo.create(
                    docPublicAcc.address.plain(),
                    ownerSigningMsg.body.fileName,
                    ownerSigningMsg.body.fileType,
                    ownerSigningMsg.body.fileHash,
                    uploadDate,
                    owner,
                    cosigners,
                    docPublicAcc,
                    ownerSigningMsg.body.uploadTxHash,
                    ownerSigningMsg.body.encryptType,
                    verifiers,
                );
                const isSigner = doc.checkIsSigner(userAccount.address.plain());
                const isVerifier = doc.checkIsVerifier(userAccount.address.plain());
                if (isSigner) {
                    if (doc.checkIsCompleted()) completed.push(doc);
                    else waiting.push(doc);
                }
                if (isVerifier) {
                    if (doc.checkIsVerified()) verified.push(doc);
                    else verifying.push(doc);
                }

                await Util.sleep(200);
                if (index > 0 && (index % 2) == 0) {
                    await Util.sleep(200);
                }
            }, () => fnStopFetching());
        }
        await fetchDocuments().catch(err => {
            console.log(err);
            this.isFetching = false;
        });
        if (fnStopFetching()) return;
        this.isFetching = false;

        return { completed, waiting, verified, verifying };
    }


    /**
     * Fetch all completely signed documents info
     * @param fnStopFetching callback return true to stop fetching
     */
    async allCompleted(userAccount: PublicAccount, fnStopFetching: any = () => false) {
        const { completed, waiting, verified, verifying } = await this.allCompletedAndWaiting(userAccount, fnStopFetching);
        return completed;
    }

    /**
     * Fetch all documents info that are waiting for other account to sign
     * @param fnStopFetching callback return true to stop fetching
     */
    async allWaiting(userAccount: PublicAccount, fnStopFetching: any = () => false) {
        const { completed, waiting, verified, verifying } = await this.allCompletedAndWaiting(userAccount, fnStopFetching);
        return waiting;
    }

    /**
     * Fetch all completely verified documents info
     * @param fnStopFetching callback return true to stop fetching
     */
    async allVerified(userAccount: PublicAccount, fnStopFetching: any = () => false) {
        const { completed, waiting, verified, verifying } = await this.allCompletedAndWaiting(userAccount, fnStopFetching);
        return verified;
    }

    /**
     * Fetch all documents info that are waiting for other account to verify
     * @param fnStopFetching callback return true to stop fetching
     */
    async allVerifying(userAccount: PublicAccount, fnStopFetching: any = () => false) {
        const { completed, waiting, verified, verifying } = await this.allCompletedAndWaiting(userAccount, fnStopFetching);
        return verifying;
    }

    /**
     * Fetch final document upload transaction
     * @param documentAccount
     */
    async fetchFinalDocumentUploadTx(documentAccount: PublicAccount) {
        const allIncomingTxs = await this.fetchAllIncomingTransactions(documentAccount);
        for (let i = 0; i < allIncomingTxs.length; i++) {
            const tx = allIncomingTxs[i];
            if (tx.type != TransactionType.TRANSFER) continue;
            const message = (tx as TransferTransaction).message.payload;
            const ssMsg = JSON.parse(message);
            if (ssMsg.privacyType && ssMsg.data && ssMsg.version) {
                if (ssMsg.data.description == 'SiriusSign Signed Document')
                    return tx;
            }
        }
        return null;
    }
}