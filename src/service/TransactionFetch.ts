import {
    NetworkType,
    PublicAccount,
    QueryParams,
    AccountHttp,
    Transaction,
    AggregateTransaction,
    TransactionType,
    Address,
    AccountInfo,
    TransactionHttp
} from "tsjs-xpx-chain-sdk";
import { Util } from "../util/Util";

export class TransactionFetch {

    constructor(
        public apiNode: string,
        public networkType: NetworkType,
        public queryPageSize: number = 10
    ) { }

    /**
     * Fetch transaction by hash
     * @param hash
     */
    fetchTransaction(hash: string) {
        const transactionHttp = new TransactionHttp(this.apiNode);
        return transactionHttp.getTransaction(hash).toPromise();
    }

    async fetchTransactionId(hash: string) {
        if (!hash) return null;
        const tx = await this.fetchTransaction(hash);
        return tx.transactionInfo.id;
    }

    fetchConfirmedTransactions(account: PublicAccount, lastId: string) {
        // console.log('fetchConfirmedTransactions: ' + lastId);
        const querryParams =  new QueryParams(this.queryPageSize, lastId);
        const accountHttp = new AccountHttp(this.apiNode);
        return accountHttp.transactions(account, querryParams).toPromise();
    }

    fetchIncomingTransactions(account: PublicAccount, lastId: string) {
        // console.log('fetchIncomingTransactions: ' + lastId);
        const querryParams =  new QueryParams(this.queryPageSize, lastId);
        const accountHttp = new AccountHttp(this.apiNode);
        return accountHttp.incomingTransactions(account, querryParams).toPromise();
    }

    fetchOutgoingTransactions(account: PublicAccount, lastId: string) {
        // console.log('fetchOutgoingTransactions: ' + lastId);
        const querryParams =  new QueryParams(this.queryPageSize, lastId);
        const accountHttp = new AccountHttp(this.apiNode);
        return accountHttp.outgoingTransactions(account, querryParams).toPromise();
    }

    /**
     * Fetch aggregate complete transaction of an account
     * @param publicAccount
     */
    async fetchAggregateCommpleteTransactions(publicAccount: PublicAccount, lastId: string) {
        const confirmedTxs = await this.fetchConfirmedTransactions(publicAccount, lastId);
        const aggCompleteTxs: AggregateTransaction[] = confirmedTxs
            .filter(tx => (tx.type == TransactionType.AGGREGATE_COMPLETE))
            .map(tx => <AggregateTransaction>tx);
        return { txs: aggCompleteTxs, lastHash: confirmedTxs[confirmedTxs.length - 1].transactionInfo.hash };
    }

    /**
     * Fetch all transactions of a kind od transation according to fnFetch
     * @param publicAccount
     * @param isReturnAll return all transactions fetched
     * @param fnFetch
     * @param fnEachFetch
     */
    async fetchAll(publicAccount: PublicAccount, isReturnAll: boolean, fnFetch: any, fnEachFetch: any) {
        let allTxs: Transaction[] = [];
        let lastId: string = null;
        let isLast: boolean = false;
        const loadAll = async () => {
            if (!isLast) {
                const txs: Transaction[] = await fnFetch(publicAccount, lastId)
                    .catch((err?: any) => { return null as any; });
                if (txs) {
                    let last;
                    if (txs.length > 0) last = txs[txs.length - 1].transactionInfo.id;
                    else last = null;
                    isLast = (txs.length < 10) || (last == lastId);
                    lastId = last;

                    fnEachFetch(txs);
                    if (isReturnAll) allTxs = [...allTxs, ...txs];
                }
                else {
                    isLast = true;
                }

                await Util.sleep(100);
                await loadAll();
            }
        }
        await loadAll();
        if (isReturnAll) return allTxs;
    }

    /**
     * Fetch all incoming transations of an account
     * @param publicAccount
     */
    async fetchAllIncomingTransactions(publicAccount: PublicAccount) {
        const allIncomingTxs = await this.fetchAll(publicAccount, true,
            (publicAccount: PublicAccount, lastId: string) => this.fetchIncomingTransactions(publicAccount, lastId),
            (txs: Transaction[]) => { return txs; });
        return allIncomingTxs;
    }

    /**
     * Fetch all outgoing transations of an account
     * @param publicAccount
     */
    async fetchAllOutgoingTransactions(publicAccount: PublicAccount) {
        const allOutgoingTxs = await this.fetchAll(publicAccount, true,
            (publicAccount: PublicAccount, lastId: string) => this.fetchOutgoingTransactions(publicAccount, lastId),
            (txs: Transaction[]) => { return txs; });
        return allOutgoingTxs;
    }

    /**
     * Fetch all aggregate complete transaction of an account
     * @param publicAccount
     */
    async fetchAllAggregateCommpleteTransactions(publicAccount: PublicAccount) {
        let allAggCompleteTxs: AggregateTransaction[] = [];
        await this.fetchAll(publicAccount, false,
            (publicAccount: PublicAccount, lastId: string) => this.fetchConfirmedTransactions(publicAccount, lastId),
            (txs: Transaction[]) => {
                txs.forEach(tx => {
                    if (tx.type == TransactionType.AGGREGATE_COMPLETE)
                        allAggCompleteTxs = [...allAggCompleteTxs, <AggregateTransaction>tx];
                });
            });
        return allAggCompleteTxs;
    }

    /**
     * Fetch public account of address
     * @param address
     */
    async fetchPublicAccount(address: Address) {
        const accountHttp = new AccountHttp(this.apiNode);
        const docAccInfo: AccountInfo = await accountHttp.getAccountInfo(address).toPromise()
            .catch(err => { console.log(err); return null; });
        const docAccPublicKey = docAccInfo.publicKey;
        const docPublicAcc = PublicAccount.createFromPublicKey(docAccPublicKey, this.networkType);
        return docPublicAcc;
    }
}