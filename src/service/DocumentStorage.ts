import * as CryptoJS from 'crypto-js';
import { NetworkType, Account, PublicAccount, Mosaic } from 'tsjs-xpx-chain-sdk';
import {
    IpfsConnection,
    BlockchainNetworkConnection,
    Converter,
    Protocol,
    ConnectionConfig,
    Uploader,
    Downloader,
    Uint8ArrayParameterData,
    UploadParameter,
    DownloadParameter
} from 'tsjs-chain-xipfs-sdk';
import { NodeUtil } from '../util/NodeUtil';
import { ConverterUtil } from '../util/ConverterUtil';
import { EncryptType } from '../model/EncryptType';
import { SiriusSignDocument } from '../model/SiriusSignDocument';
import { FileInfo } from './DocumentSigning';

export class DocumentStorage {
    private blockchainConnection: BlockchainNetworkConnection;
    private ipfsConnection: IpfsConnection;

    // Connection Config
    private conectionConfig: ConnectionConfig;

    // Dowloader, Uploader
    private uploader: Uploader;
    private downloader: Downloader;


    constructor(apiNode: string, ipfsNode: string, networkType: NetworkType) {
        const blockchainNetworkType = Converter.toBlockchainNetworkType(Converter.getBlockchainNetworkType(networkType));
        const chainNode = NodeUtil.split(apiNode);
        this.blockchainConnection = new BlockchainNetworkConnection(
            blockchainNetworkType, // the network type
            chainNode.domain, // the rest api base endpoint
            chainNode.port, // the optional websocket end point
            (chainNode.protocol == 'https') ? Protocol.HTTPS : Protocol.HTTP
        );

        const storageNode = NodeUtil.split(ipfsNode);
        this.ipfsConnection = new IpfsConnection(
            storageNode.domain, // the host or multi address
            storageNode.port, // the port number
            (storageNode.protocol == 'https') ? { protocol: 'https' } : { protocol: 'http' } // the optional protocol
        );

        this.conectionConfig = ConnectionConfig.createWithLocalIpfsConnection(this.blockchainConnection, this.ipfsConnection);
        this.uploader = new Uploader(this.conectionConfig);
        this.downloader = new Downloader(this.conectionConfig);
    }

    /**
     * Create upload params builder
     * @param document
     */
    private createParamsBuilder(fileName: string, fileType: string, fileData: Uint8Array, fileHash: string, documentAccount: Account, owner: Account) {
        const documentAccountPriv = CryptoJS.AES.encrypt(documentAccount.privateKey, owner.privateKey);
        const documentAccountEncPriv = ConverterUtil.base64ToHex(documentAccountPriv.toString()).toUpperCase();
        const metaData = new Map<string, string>([
            ['SFH', fileHash],
            ['SDA', documentAccountEncPriv]
        ]);
        const metaParams = Uint8ArrayParameterData.create(fileData, fileName, '', fileType, metaData);
        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, owner.privateKey);
        return uploadParamsBuilder;
    }

    /**
     * Upload file from Sirius Sign Document
     */
    uploadDocument(document: SiriusSignDocument, owner: Account, password?: string) {
        const privateKey = owner.privateKey;
        const publicKey = owner.publicKey;
        const fileName = document.fileName;
        const fileType = document.fileType;
        const fileHash = document.fileHash;
        const fileData = document.fileData;

        const uploadParamsBuilder = this.createParamsBuilder(fileName, fileType, fileData, fileHash, document.documentAccount, owner);

        let uploadParams: UploadParameter;
        const encryptType = document.info.encryptType;
        switch (encryptType) {
            case EncryptType.KEYPAIR: {
                uploadParams = uploadParamsBuilder
                    .withRecipientPublicKey(document.documentAccount.publicKey)
                    .withTransactionMosaics([])
                    .withNemKeysPrivacy(privateKey, publicKey)
                    .build();
                break;
            }
            case EncryptType.PASSWORD: {
                if (!password) throw new Error('I02: password is required');
                uploadParams = uploadParamsBuilder
                    .withRecipientPublicKey(document.documentAccount.publicKey)
                    .withTransactionMosaics([])
                    .withPasswordPrivacy(password)
                    .build();
                break;
            }
            case EncryptType.PLAIN: {
                uploadParams = uploadParamsBuilder
                    .withRecipientPublicKey(document.documentAccount.publicKey)
                    .withTransactionMosaics([])
                    .build();
                break;
            }
            default: {
                uploadParams = uploadParamsBuilder
                    .withRecipientPublicKey(document.documentAccount.publicKey)
                    .withTransactionMosaics([])
                    .build();
                break;
            }
        }

        return this.uploader.upload(uploadParams);
    }

    /**
     * Download file from file hash
     */
    downloadDocument(hash: string, encryptType: EncryptType, decryptParam?: Account | string) {
        const downloadParamsBuilder = DownloadParameter.create(hash);
        let downloadParams: DownloadParameter;
        switch (encryptType) {
            case EncryptType.KEYPAIR: {
                if (!decryptParam || !(decryptParam instanceof Account)) throw new Error('I03: Owner Account as decryptParam is required');
                const owner = decryptParam as Account;
                const privateKey = owner ? owner.privateKey : '';
                const publicKey = owner ? owner.publicKey : '';
                downloadParams = downloadParamsBuilder.withNemKeysPrivacy(privateKey, publicKey).build();
                break;
            }
            case EncryptType.PASSWORD: {
                if (!decryptParam || (typeof (decryptParam) != 'string')) throw new Error('I04: Password as decryptParam is required');
                const password = decryptParam as string;
                downloadParams = downloadParamsBuilder.withPasswordPrivacy(password).build();
                break;
            }
            case EncryptType.PLAIN: {
                downloadParams = downloadParamsBuilder.build();
                break;
            }
            default: {
                downloadParams = downloadParamsBuilder.build();
                break;
            }
        }
        return this.downloader.download(downloadParams);
    }

    /**
     * Upload signature image
     * @param signatureImg
     */
    uploadCompletedDoc(originFilename: string, fileType: string, originFileHash: string, completedFileData: Uint8Array, uploaderAccount: Account, documentPublicAccount: PublicAccount, encryptType: EncryptType, password?: string) {
        const fileContent = completedFileData;
        const fileName = originFilename.substr(0, originFilename.lastIndexOf('.')) + '-' + 'signed' + originFilename.substr(originFilename.lastIndexOf('.'));
        const fileDescription = 'SiriusSign Signed Document';
        const completedDocDataUri = 'data:' + fileType + ';base64,' + ConverterUtil.uint8ArrayToBase64(completedFileData);
        const cFileHash = ConverterUtil.base64ToHex(CryptoJS.SHA256(completedDocDataUri).toString()).toUpperCase();
        const oFileHash = originFileHash;
        const metaData = new Map<string, string>([
            ['SCH', cFileHash],
            ['SFH', oFileHash]
        ]);

        const privateKey = uploaderAccount.privateKey;
        const publicKey = uploaderAccount.publicKey;

        const metaParams = Uint8ArrayParameterData.create(
            fileContent,
            fileName,
            fileDescription,
            fileType,
            metaData
        );
        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
        let uploadParams: UploadParameter;
        switch (encryptType) {
            case EncryptType.KEYPAIR: {
                uploadParams = uploadParamsBuilder
                    .withRecipientPublicKey(documentPublicAccount.publicKey)
                    .withTransactionMosaics([])
                    .withNemKeysPrivacy(privateKey, publicKey)
                    .build();
                break;
            }
            case EncryptType.PASSWORD: {
                if (!password) throw new Error('I02: password is required');
                uploadParams = uploadParamsBuilder
                    .withRecipientPublicKey(documentPublicAccount.publicKey)
                    .withTransactionMosaics([])
                    .withPasswordPrivacy(password)
                    .build();
                break;
            }
            case EncryptType.PLAIN: {
                uploadParams = uploadParamsBuilder
                    .withRecipientPublicKey(documentPublicAccount.publicKey)
                    .withTransactionMosaics([])
                    .build();
                break;
            }
            default: {
                uploadParams = uploadParamsBuilder
                    .withRecipientPublicKey(documentPublicAccount.publicKey)
                    .withTransactionMosaics([])
                    .build();
                break;
            }
        }

        return this.uploader.upload(uploadParams);
    }

    /**
     * Upload signature image
     */
    uploadSignatureForDocSigning(fileName: string, fileType: string, fileData: Uint8Array, userAccount: Account, documentPublicAccount: PublicAccount) {
        const fileDescription = 'SiriusSign Signature Image';
        const signatureImg = ConverterUtil.base64ToDataURI(ConverterUtil.uint8ArrayToBase64(fileData), fileType);
        const fileHash = CryptoJS.SHA256(signatureImg).toString().toLocaleUpperCase();
        const metaData = new Map<string, string>([
            ['SSH', fileHash]
        ]);

        const privateKey = userAccount.privateKey;

        const metaParams = Uint8ArrayParameterData.create(
            fileData,
            fileName,
            fileDescription,
            fileType,
            metaData
        );
        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, privateKey);
        const uploadParams = uploadParamsBuilder
            .withRecipientPublicKey(documentPublicAccount.publicKey)
            .withTransactionMosaics([])
            .build();

        return this.uploader.upload(uploadParams);
    }

    upload(sender: Account, receiver: PublicAccount, fileInfo: FileInfo, decription: string, metadata: Map<string, string>, mosaics: Mosaic[], isSecureMessage: boolean) {
        const fileData = fileInfo.fileData;
        const fileName = fileInfo.fileName;
        const fileType = fileInfo.fileType;
        const metaParams = Uint8ArrayParameterData.create(fileData, fileName, decription, fileType, metadata);
        const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, sender.privateKey);
        const uploadParams = uploadParamsBuilder
            .withRecipientPublicKey(receiver.publicKey)
            .withTransactionMosaics(mosaics)
            .withUseBlockchainSecureMessage(isSecureMessage)
            .build();
        return this.uploader.upload(uploadParams);
    }
}