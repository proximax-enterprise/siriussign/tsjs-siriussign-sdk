import { Account, NetworkType, Transaction, TransactionStatusError, SignedTransaction, TransferTransaction, Mosaic, UInt64 } from "tsjs-xpx-chain-sdk";
import { SiriusSignDocument } from "../model";
import { KeyValueObject } from "../model/SiriusSignMessage";
import { DocumentStorage } from "./DocumentStorage";
import { SiriusSignTransaction } from "./SiriusSignTransaction";
import { TransactionListener } from "./TransactionListener";
import { DocumentFetch } from "./DocumentFetch";
import { ConverterUtil } from "../util/ConverterUtil";
import { DEADLINE_ADJUSTED_HOURS } from "../config/constant";


export interface FileInfo {
    fileName: string,
    fileType: string,
    fileData: Uint8Array
}

export class DocumentSigning {

    state: number = 0;
    isDone: boolean;
    isConfirmed: boolean;
    status: TransactionStatusError | string;

    constructor(
        public appID: string,
        public userAccount: Account,
        public apiNode: string,
        public ipfsNode: string,
        public networkGenerationHash: string,
        public networkType: NetworkType,
        public websockketInject?: any
    ) {
        this.resetState();
    }

    private resetState() {
        this.state = 0;
        this.isDone = false;
        this.isConfirmed = false;
    }

    /**
     * Upload file
     *
     * If failed, set process done, status failed by error
     */
    async uploadSignDoc(document: SiriusSignDocument, completedDocumentData?: Uint8Array, password?: string) {
        const documentStorage = new DocumentStorage(this.apiNode, this.ipfsNode, this.networkType)

        const uploadRes = await documentStorage.uploadDocument(document, this.userAccount, password)
            .catch(err => { throw new Error('Cannot upload the document') });

        if (completedDocumentData)
            documentStorage.uploadCompletedDoc(
                document.fileName,
                document.fileType,
                document.fileHash,
                completedDocumentData,
                this.userAccount,
                document.info.documentAccount,
                document.info.encryptType,
                password
            ).catch(err => { throw new Error('Cannot upload the document.') });;

        return uploadRes;
    }

    /**
     * Single sign with new process
     */
    async singleSign(document: SiriusSignDocument, signatureImage?: FileInfo, password?: string, meta?: KeyValueObject) {
        const siriusSignTransaction = new SiriusSignTransaction(this.appID, this.userAccount, this.networkGenerationHash, this.networkType);
        const transactionListner = new TransactionListener(this.apiNode, this.websockketInject);
        const documentStorage = new DocumentStorage(this.apiNode, this.ipfsNode, this.networkType);
        this.resetState();
        if (document.info.cosigners.length > 0) throw new Error('S01: Cannot run singleSign with a multi-sign document');

        // Create document account
        document.setDocumentAccount(Account.generateNewAccount(this.networkType));

        // Set Restriction
        const signedRestrictionTx = siriusSignTransaction.createRestriction(document);
        const restrictTx: Transaction = await transactionListner.waitForTransactionConfirmed(
            document.documentAccount.address,
            signedRestrictionTx.hash,
            () => transactionListner.announceTransaction(signedRestrictionTx))
            .catch(err => {
                this.isDone = true;
                this.status = err;
                return null;
            });
        if (!restrictTx) return;
        this.state++;

        // Upload file
        if (!document.completedDocumentData) document.setFinalDocumentData(document.fileData);
        const uploadTx = await this.uploadSignDoc(document, document.completedDocumentData, password);
        if (!uploadTx) return;
        document.setUploadTxHash(uploadTx.transactionHash);
        this.state++;

        // Upload signature
        if (signatureImage) {
            const signatureUploadTx = await documentStorage.uploadSignatureForDocSigning(
                signatureImage.fileName,
                signatureImage.fileType,
                signatureImage.fileData,
                this.userAccount,
                document.documentAccount.publicAccount
            ).catch(err => {
                this.isDone = true;
                this.status = err.status;
                return null;
            });
            if (!signatureUploadTx) return;
            document.setSignatureUploadTxhash(this.userAccount.publicAccount, signatureUploadTx.transactionHash);
        }
        else {
            document.setSignatureUploadTxhash(this.userAccount.publicAccount, null);
        }
        this.state++;

        // Sign transaction
        // doc.setTxHash is performed inside create doc signing tx
        const signedDocSigningTx = siriusSignTransaction.createDocSigningTransaction(document, [], meta);
        const docSigningTx = await transactionListner.waitForTransactionConfirmed(this.userAccount.address, signedDocSigningTx.hash,
            () => transactionListner.announceTransaction(signedDocSigningTx))
            .catch(err => {
                this.isDone = true;
                this.status = err;
                return null;
            });
        if (!docSigningTx) return;
        document.setSignTxHash(this.userAccount.publicAccount, docSigningTx.transactionInfo.hash, ConverterUtil.localTimeToDate(docSigningTx.deadline.value, DEADLINE_ADJUSTED_HOURS));
        document.info.checkIsCompleted();
        this.state++;

        this.isDone = true;
        this.isConfirmed = true;
    }

    /**
     * Run multi sign task
     */
    async multiSign(document: SiriusSignDocument, mosaics: Mosaic[] = [], signatureImage?: FileInfo, signingMeta?: KeyValueObject, cosignerMetaArray?: KeyValueObject[], password?: string) {
        const siriusSignTransaction = new SiriusSignTransaction(this.appID, this.userAccount, this.networkGenerationHash, this.networkType);
        const transactionListner = new TransactionListener(this.apiNode, this.websockketInject);
        const documentStorage = new DocumentStorage(this.apiNode, this.ipfsNode, this.networkType);
        this.resetState();
        if (document.info.cosigners.length == 0) throw new Error('S02: Cannot run multiSign with a single-sign document');

        // Create document account
        document.setDocumentAccount(Account.generateNewAccount(this.networkType));

        // Set Restriction
        const signedRestrictionTx = siriusSignTransaction.createRestriction(document);
        const restrictTx: Transaction = await transactionListner.waitForTransactionConfirmed(
            document.documentAccount.address,
            signedRestrictionTx.hash,
            () => transactionListner.announceTransaction(signedRestrictionTx))
            .catch(err => {
                this.isDone = true;
                this.status = err;
                return null;
            });
        if (!restrictTx) return;
        this.state++;

        // Upload file
        const uploadTx = await this.uploadSignDoc(document, document.completedDocumentData, password);
        if (!uploadTx) return;
        document.setUploadTxHash(uploadTx.transactionHash);
        this.state++;

        // Upload signature
        if (signatureImage) {
            const signatureUploadTx = await documentStorage.uploadSignatureForDocSigning(
                signatureImage.fileName,
                signatureImage.fileType,
                signatureImage.fileData,
                this.userAccount,
                document.documentAccount.publicAccount
            ).catch(err => {
                this.isDone = true;
                this.status = err.status;
                return null;
            });
            if (!signatureUploadTx) return;
            document.setSignatureUploadTxhash(this.userAccount.publicAccount, signatureUploadTx.transactionHash);
        }
        else {
            document.setSignatureUploadTxhash(this.userAccount.publicAccount, null);
        }
        this.state++;

        // Sign transaction
        const signedDocSigningTx = siriusSignTransaction.createDocSigningTransaction(document, mosaics, signingMeta);
        const docSigningTx: Transaction = await transactionListner.waitForTransactionConfirmed(this.userAccount.address, signedDocSigningTx.hash,
            () => transactionListner.announceTransaction(signedDocSigningTx))
            .catch(err => {
                this.isDone = true;
                this.status = err;
                return null;
            });
        if (!docSigningTx) return;
        document.setSignTxHash(this.userAccount.publicAccount, docSigningTx.transactionInfo.hash, ConverterUtil.localTimeToDate(docSigningTx.deadline.value, DEADLINE_ADJUSTED_HOURS));
        document.info.checkIsCompleted();
        this.state++;

        // Send notification
        const totalCosignersVerifiers = document.info.cosigners.length + document.info.verifiers.length;
        const notiMosaics = mosaics.map(mosaic => {
            return new Mosaic(mosaic.id, UInt64.fromUint(mosaic.amount.compact() / totalCosignersVerifiers));
        });
        const signedNotiTx = siriusSignTransaction.createMultiSigningNotificationTransaction(document, notiMosaics, cosignerMetaArray);
        const aggNotiTx: Transaction = await transactionListner.waitForTransactionConfirmed(document.documentAccount.address, signedNotiTx.hash,
            () => transactionListner.announceTransaction(signedNotiTx))
            .catch(err => {
                this.isDone = true;
                this.status = err;
                return null;
            });;
        if (!aggNotiTx) return;
        this.state++;

        this.isDone = true;
        this.isConfirmed = true;
    }

    /**
     * Run Cosign task
     */
    async cosign(document: SiriusSignDocument, mosaics: Mosaic[] = [], signatureImage?: FileInfo, meta?: KeyValueObject, password?: string) {
        const siriusSignTransaction = new SiriusSignTransaction(this.appID, this.userAccount, this.networkGenerationHash, this.networkType);
        const transactionListner = new TransactionListener(this.apiNode, this.websockketInject);
        const documentStorage = new DocumentStorage(this.apiNode, this.ipfsNode, this.networkType);
        const documentFetch = new DocumentFetch(this.appID, this.apiNode, this.networkType);
        this.resetState();

        // Upload signature
        // let userSignatureIndex = this.selectedDocInfo.signerSignatures
        //     .map(signature => signature.publicKey)
        //     .indexOf(this.account.publicKey);
        // if (userSignatureIndex < 0) {
        //     userSignatureIndex = this.selectedDocInfo.signerSignatures
        //     .map(signature => signature.publicKey)
        //     .indexOf(this.account.address.plain());
        // }

        if (signatureImage) {
            this.state = 0;
            const signatureUploadTx = await documentStorage.uploadSignatureForDocSigning(
                signatureImage.fileName,
                signatureImage.fileType,
                signatureImage.fileData,
                this.userAccount,
                document.info.documentAccount
            ).catch(err => {
                this.isDone = true;
                this.status = err.status;
                return null;
            });
            if (!signatureUploadTx) return;
            // this.selectedDocInfo.signaturesUploadTxHash.push(signatureUploadTx.transactionHash);
            document.setSignatureUploadTxhash(this.userAccount.publicAccount, signatureUploadTx.transactionHash);
        }
        else {
            document.setSignatureUploadTxhash(this.userAccount.publicAccount, null);
        }
        this.state++;

        // Sign transaction
        let signedDocSigningTx: SignedTransaction;
        signedDocSigningTx = siriusSignTransaction.createDocSigningTransaction(document, mosaics, meta);

        const docSigningTx: Transaction = await transactionListner.waitForTransactionConfirmed(this.userAccount.address, signedDocSigningTx.hash,
            () => transactionListner.announceTransaction(signedDocSigningTx))
            .catch(err => {
                this.isDone = true;
                this.status = err;
                return null;
            });
        if (!docSigningTx) return;
        document.setSignTxHash(this.userAccount.publicAccount, docSigningTx.transactionInfo.hash, ConverterUtil.localTimeToDate(docSigningTx.deadline.value, DEADLINE_ADJUSTED_HOURS));
        document.info.checkIsCompleted();
        this.state++;

        // Upload completed document
        const allDocumentSigningTxs = await documentFetch.allDocumentSigningTransactionsByDocumentAccount(document.info.documentAccount.publicKey);
        const sortCondition = (a: TransferTransaction, b: TransferTransaction) => {
            if (a.transactionInfo.id > b.transactionInfo.id) return -1;
            else return 1;
        }
        // allDocumentSigningTxs.sort((tx1, tx2) => sortCondition(tx1, tx2));
        // console.log(allDocumentSigningTxs);
        // if (this.selectedDocInfo.cosignatures.length == this.selectedDocInfo.cosigners.length - 1)
        //     this.uploadStorage.uploadCompletedDoc(this.completedFileUint8Array, this.name, this.selectedDocInfo.documentAccount, this.selectedDocInfo.isEncrypt, this.selectedDocInfo);

        if (
            (allDocumentSigningTxs[0].signer.publicKey == this.userAccount.publicKey) &&
            (document.info.cosigners.length == allDocumentSigningTxs.length - 1)
        ) {
            await documentStorage.uploadCompletedDoc(
                document.fileName,
                document.fileType,
                document.fileHash,
                document.completedDocumentData,
                this.userAccount,
                document.info.documentAccount,
                document.info.encryptType,
                password
            );
        }
        this.state++;

        this.isDone = true;
        this.isConfirmed = true;
    }

    /**
     * Run verify task
     */
    async verify(document: SiriusSignDocument, mosaics: Mosaic[] = [], meta: KeyValueObject) {
        const siriusSignTransaction = new SiriusSignTransaction(this.appID, this.userAccount, this.networkGenerationHash, this.networkType);
        const transactionListner = new TransactionListener(this.apiNode, this.websockketInject);
        this.resetState();

        const isCompleted = document.info.checkIsCompleted();
        if (!isCompleted) throw new Error('S03: Cannot verify an unsigned document')

        const signedVerifyTx = siriusSignTransaction.createDocSigningTransaction(document, mosaics, meta);
        const docVerifyTx: Transaction = await transactionListner.waitForTransactionConfirmed(this.userAccount.address, signedVerifyTx.hash,
            () => transactionListner.announceTransaction(signedVerifyTx)
        ).catch(err => {
            this.status = err.message;
            return null;
        });
        if (!docVerifyTx) return;
        this.state++;

        this.isDone = true;
        this.isConfirmed = true;
    }
}