import {
    AggregateTransaction,
    TransactionType,
    TransferTransaction,
    AccountHttp,
    Address,
    Transaction,
    PublicAccount,
    NetworkType
} from "tsjs-xpx-chain-sdk";
import { TransactionFetch } from "./TransactionFetch";
import { SiriusSignMessage } from "../model/SiriusSignMessage";
import {
    SiriusSignMessageType,
    SiriusSignDocumentSigningMessage,
    SiriusSignCosignerNotifyingMessage,
    SiriusSignDocumentVerifyingMessage,
    SiriusSignVerifierNotifyingMessage,
    SignerInfo,
    ConverterUtil,
} from "..";
import { PUBLIC_KEY_NOT_FOUND } from "../config/constant";

interface RawAccountInfo {
    publicKey: string,
    address: Address
}

export class DocumentInfoService extends TransactionFetch {

    public appID: string;
    private useStrict: boolean = false;

    constructor(
        apiNode: string,
        networkType: NetworkType,
        queryPageSize: number = 10
    ) {
        super(apiNode, networkType, queryPageSize);
    }

    /**
     * Set functions to filter Sirius Sign transaction message with given appID
     * @param appID
     */
    setStrictFilter(appID: string) {
        this.appID = appID;
        this.useStrict = true;
    }

    /**
     * Clear strict filter
     */
    clearStrictFilter() {
        this.useStrict = false;
    }

    /**
     * Check if app ID in message is the same to given app ID if strict filter is set
     * @param appID
     */
    protected checkStrict(appID: string) {
        return (this.useStrict && appID == this.appID) || (!this.useStrict);
    }

    /**
     * Parse payload of transaction to Sirius Sign message
     * @param payload
     */
    parseSiriusSignMessage(payload: string) {
        const errorResult: SiriusSignMessage = null;
        try {
            const type = SiriusSignMessage.getType(payload);
            switch (type) {
                case SiriusSignMessageType.SIGN: {
                    return SiriusSignDocumentSigningMessage.createFromPayload(payload);
                }
                case SiriusSignMessageType.SIGN_NOTIFY: {
                    return SiriusSignCosignerNotifyingMessage.createFromPayload(payload);
                }
                case SiriusSignMessageType.VERIFY: {
                    return SiriusSignDocumentVerifyingMessage.createFromPayload(payload);
                }
                case SiriusSignMessageType.VERIFY_NOTIFY: {
                    return SiriusSignVerifierNotifyingMessage.createFromPayload(payload);
                }
                default: return errorResult;
            }
        }
        catch (err) {
            return errorResult;
        }
    }

    /**
     * Filter Document Signing Notifications from an array of Aggregate Commplete Transactions
     * @param aggCompleteTxs
     */
    protected filterDocumentSigningNotifications(aggCompleteTxs: AggregateTransaction[]) {
        let documentSigningNotifications: AggregateTransaction[] = [];
        aggCompleteTxs.forEach(aggTx => {
            const checkTx = aggTx.innerTransactions[0];
            let isNotification = false;
            if (checkTx.type == TransactionType.TRANSFER) {
                const info = this.parseSiriusSignMessage((<TransferTransaction>checkTx).message.payload);
                if (info && (this.checkStrict(info.header.appID)) && (info.header.messageType == SiriusSignMessageType.SIGN_NOTIFY))
                    isNotification = true;
            }

            if (isNotification) {
                documentSigningNotifications.push(aggTx);
            }
        });
        return documentSigningNotifications;
    }

    /**
     * Filter Document Signing Transaction from an array of transactions
     * @param aggCompleteTxs
     */
    protected filterDocumentSigningTransations(txs: Transaction[]) {
        let documentSigningTransactions: TransferTransaction[] = [];
        txs.forEach(tx => {
            if (tx.type == TransactionType.TRANSFER) {
                const info = this.parseSiriusSignMessage((<TransferTransaction>tx).message.payload);
                if (info && (this.checkStrict(info.header.appID)) &&
                    ((info.header.messageType == SiriusSignMessageType.SIGN) || (info.header.messageType == SiriusSignMessageType.VERIFY))) {
                    documentSigningTransactions = [...documentSigningTransactions, <TransferTransaction>tx];
                }
            }
        });
        return documentSigningTransactions;
    }

    /**
     * Fetch info of signer and verifier from noti aggregate tx
     * @param notiAggTx
     * @param userAccount
     */
    protected async getCosignerVerifiersFromNotiAggTx(notiAggTx: AggregateTransaction, userAccount?: PublicAccount) {
        // Check cosigners
        let cosigners: SignerInfo[] = [];
        let verifiers: SignerInfo[] = [];
        let cosignerInfos: RawAccountInfo[] = [];
        let verifierInfos: RawAccountInfo[] = [];
        if (notiAggTx) {
            [cosignerInfos, verifierInfos] = await this.fetchDoucmentSignersAndVerifiersFromNotiTx(notiAggTx);
            cosigners = cosignerInfos.map(info => {
                const cosigner: SignerInfo = {
                    publicKey: info.publicKey,
                    address: info.address.plain(),
                    isSigned: false,
                    signDate: null,
                    signTxHash: null,
                    signatureUploadTxHash: null
                }
                if (
                    userAccount &&
                    cosigner.publicKey == PUBLIC_KEY_NOT_FOUND &&
                    cosigner.address == userAccount.address.plain()
                ) {
                    cosigner.publicKey = userAccount.publicKey
                }
                return cosigner;
            });
            verifiers = verifierInfos.map(info => {
                const verifier: SignerInfo = {
                    publicKey: info.publicKey,
                    address: info.address.plain(),
                    isSigned: false,
                    signDate: null,
                    signTxHash: null,
                    signatureUploadTxHash: null
                }
                if (
                    userAccount &&
                    verifier.publicKey == PUBLIC_KEY_NOT_FOUND &&
                    verifier.address == userAccount.address.plain()
                ) {
                    verifier.publicKey = userAccount.publicKey
                }
                return verifier;
            });
        }

        return { cosigners, verifiers };
    }

    /**
     * Fetch signer public key, address of cosigners and verifiers from agg noti tx
     * @param aggNotiTx
     */
    protected async fetchDoucmentSignersAndVerifiersFromNotiTx(aggNotiTx: AggregateTransaction) {
        const accountHttp = new AccountHttp(this.apiNode);
        const notiTxs = aggNotiTx.innerTransactions.map(tx => <TransferTransaction>tx);

        const recvAddresses = notiTxs.map(tx => tx.recipient);
        const recvInfos = await accountHttp.getAccountsInfo(<Address[]>recvAddresses).toPromise();
        const recvs = recvInfos.map(info => { return { publicKey: info.publicKey, address: info.address }; })

        let cosignerAddresses: Address[] = [];
        let verifierAddresses: Address[] = [];
        notiTxs.forEach(tx => {
            const ssMsg = this.parseSiriusSignMessage(tx.message.payload);
            if (ssMsg.header.messageType == SiriusSignMessageType.SIGN_NOTIFY) cosignerAddresses.push(<Address>tx.recipient);
            if (ssMsg.header.messageType == SiriusSignMessageType.VERIFY_NOTIFY) verifierAddresses.push(<Address>tx.recipient);
        });

        const cosigners: RawAccountInfo[] = recvs.filter(recv => cosignerAddresses.map(addr => addr.plain()).includes(recv.address.plain()));
        const verifiers: RawAccountInfo[] = recvs.filter(recv => verifierAddresses.map(addr => addr.plain()).includes(recv.address.plain()));

        return [cosigners, verifiers]
    }

    /**
     * Get signed info: signer pulickey, verified publickey, owner info, signature upload tx hash
     * from incoming transactions of a document account
     * @param incomingTxs
     */
    protected getSignedSignerInfo(incomingTxs: Transaction[], userAccount?: PublicAccount) {
        // Filter SIGN transactions to get signatures
        let owner: SignerInfo;
        let ownerSigningMsg: SiriusSignDocumentSigningMessage;
        let ownerSigningTx: TransferTransaction;
        let userSigningTx: TransferTransaction = null;
        let cosigners: SignerInfo[] = [];
        let verifiers: SignerInfo[] = [];
        incomingTxs.forEach(tx => {
            if (tx.type == TransactionType.TRANSFER) {
                const transferTx = <TransferTransaction>tx;
                const txMsg = this.parseSiriusSignMessage(transferTx.message.payload);
                if (txMsg && (txMsg.header.messageType == SiriusSignMessageType.SIGN)) {
                    const signTxMsg = <SiriusSignDocumentSigningMessage>txMsg;
                    const signer = {
                        publicKey: transferTx.signer.publicKey,
                        address: transferTx.signer.address.plain(),
                        isSigned: true,
                        signDate: ConverterUtil.localTimeToDate(transferTx.deadline.value, -2),
                        signTxHash: transferTx.transactionInfo.hash,
                        signatureUploadTxHash: signTxMsg.body.signatureUploadTxHash
                    }
                    if (signTxMsg.body.isOwner) {
                        owner = signer;
                        ownerSigningMsg = signTxMsg;
                        ownerSigningTx = transferTx;
                    }
                    else {
                        cosigners.push(signer);
                    }
                    if (userAccount && (transferTx.signer.publicKey == userAccount.publicKey))
                        userSigningTx = transferTx;
                }
                if (txMsg && (txMsg.header.messageType == SiriusSignMessageType.VERIFY)) {
                    const verifyTxMsg = <SiriusSignDocumentVerifyingMessage>txMsg;
                    const verifier = {
                        publicKey: transferTx.signer.publicKey,
                        address: transferTx.signer.address.plain(),
                        isSigned: true,
                        signDate: ConverterUtil.localTimeToDate(transferTx.deadline.value, -2),
                        signTxHash: transferTx.transactionInfo.hash,
                        signatureUploadTxHash: null as string
                    }
                    verifiers.push(verifier);
                }
            }
        });
        return {
            owner: owner,
            ownerSigningMsg: ownerSigningMsg,
            ownerSigningTx: ownerSigningTx,
            userSigningTx: userSigningTx,
            cosigners: cosigners,
            verifiers: verifiers
        }
    }

    /**
     * Update signers info whow signed doc
     * @param signers
     * @param signeds
     */
    protected updateSigners(signers: SignerInfo[], signeds: SignerInfo[]) {
        signers.forEach(signer => {
            const idx = signeds.map(signed => signed.address).indexOf(signer.address);
            if (idx < 0) return;
            signer.isSigned = signeds[idx].isSigned;
            signer.signDate = signeds[idx].signDate;
            signer.signTxHash = signeds[idx].signTxHash;
            signer.signatureUploadTxHash = signeds[idx].signatureUploadTxHash;
        });
        return signers;
    }

    /**
     * Check Document Signing Notifications from an array of Aggregate Commplete Transactions
     * @param transaction
     */
    checkDocumentSigningNotifications(transaction: Transaction) {
        if (transaction.type == TransactionType.AGGREGATE_COMPLETE) {
            const aggTx = transaction as AggregateTransaction;
            const checkTx = aggTx.innerTransactions[0];
            let isNotification = false;
            if (checkTx.type == TransactionType.TRANSFER) {
                const info = this.parseSiriusSignMessage((<TransferTransaction>checkTx).message.payload);
                if (info && (this.checkStrict(info.header.appID)) && (info.header.messageType == SiriusSignMessageType.SIGN_NOTIFY))
                    isNotification = true;
            }

            if (isNotification) {
                return aggTx.signer;
            }
        }
        return null;
    }
}