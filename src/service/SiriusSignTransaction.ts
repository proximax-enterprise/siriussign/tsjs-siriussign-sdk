import {
    Account,
    AccountRestrictionModification,
    RestrictionModificationType,
    Address,
    NetworkType,
    AccountAddressRestrictionModificationTransaction,
    Deadline,
    RestrictionType,
    UInt64,
    PlainMessage,
    TransferTransaction,
    AggregateTransaction,
    Mosaic
} from "tsjs-xpx-chain-sdk";
import { SiriusSignDocument, SiriusSignDocumentSigningMessage, SiriusSignCosignerNotifyingMessage, SiriusSignVerifierNotifyingMessage, SiriusSignDocumentVerifyingMessage } from "../model";
import { KeyValueObject } from "../model/SiriusSignMessage";

export class SiriusSignTransaction {

    constructor(
        public appID: string,
        public userAccount: Account,
        public networkGenerationHash: string,
        public networkType: NetworkType
    ) { }

    /**
     * Create account restriction transaction to allow signers and verifiers to send transaction to document account
     */
    createRestriction(document: SiriusSignDocument) {
        const signers = [document.info.owner, ...document.info.cosigners, ...document.info.verifiers];
        const restrictionMods = signers.map(signer => {
            return AccountRestrictionModification.createForAddress(RestrictionModificationType.Add, Address.createFromPublicKey(signer.publicKey, this.networkType));
        });
        const accountRestrictionTransaction = AccountAddressRestrictionModificationTransaction.create(
            Deadline.create(),
            RestrictionType.AllowAddress,
            restrictionMods,
            this.networkType,
            UInt64.fromUint(0)
        );
        const signedAccountRestrictionTransaction = document.documentAccount.sign(accountRestrictionTransaction, this.networkGenerationHash);
        return signedAccountRestrictionTransaction;
    }

    /**
     * Create SIGN transaction to sign a document
     */
    createDocSigningTransaction(document: SiriusSignDocument, mosaics: Mosaic[] = [], meta?: KeyValueObject) {
        const isOwner = document.info.owner.publicKey == this.userAccount.publicKey;
        const ssMessage = new SiriusSignDocumentSigningMessage(
            this.appID,
            document.fileHash,
            document.fileName,
            document.fileType,
            isOwner,
            document.info.uploadTxHash,
            document.info.encryptType,
            document.signatureUploadTxHash,
            meta
        );

        const message = ssMessage.toPlainMessage();

        const docSigningTransaction = TransferTransaction.create(
            Deadline.create(),
            document.info.documentAccount.address,
            mosaics,
            message,
            this.networkType,
            UInt64.fromUint(0)
        );

        const networkGenerationHash = this.networkGenerationHash;
        const signedDocSigningTransaction = this.userAccount.sign(docSigningTransaction, networkGenerationHash);

        return signedDocSigningTransaction;
    }

    /**
     * Create aggregate complete transaction that contains NOTIFY transfer transaction to cosigners and verifiers
     * @param document 
     * @param signersSignaturesInfo 
     */
    createMultiSigningNotificationTransaction(document: SiriusSignDocument, mosaics: Mosaic[], metaArray?: KeyValueObject[]) {
        // Create cosign notify
        const ssCosignMessages = document.info.cosigners.map((cosigner, index) => {
            return new SiriusSignCosignerNotifyingMessage(
                this.appID,
                document.info.uploadTxHash,
                metaArray ? metaArray[index] : undefined
            );
        });

        const messages = ssCosignMessages.map(ssMessage => {
            return ssMessage.toPlainMessage();
        });

        const aggCosignNotificationTransactions = messages.map((message, index) => {
            const cosignerPublicKey = document.info.cosigners[index].publicKey;
            return TransferTransaction.create(
                Deadline.create(),
                Address.createFromPublicKey(cosignerPublicKey, this.networkType),
                mosaics,
                message,
                this.networkType,
                UInt64.fromUint(0)
            ).toAggregate(document.documentAccount.publicAccount);
        });

        // Create verify notify
        const ssVerifyMessage = new SiriusSignVerifierNotifyingMessage(
            this.appID,
            document.info.uploadTxHash,
        );

        const verifyMessage = PlainMessage.create(ssVerifyMessage.toMessage());

        const aggVerifyNotificationTransactions = document.info.verifiers.map(verifier =>
            TransferTransaction.create(
                Deadline.create(),
                Address.createFromPublicKey(verifier.publicKey, this.networkType),
                mosaics,
                verifyMessage,
                this.networkType,
                UInt64.fromUint(0)
            ).toAggregate(document.documentAccount.publicAccount)
        );

        // Notify transactions
        const aggNotificationTransactions = [...aggCosignNotificationTransactions, ...aggVerifyNotificationTransactions];
        const aggTransaction = AggregateTransaction.createComplete(
            Deadline.create(),
            aggNotificationTransactions,
            this.networkType,
            [],
            UInt64.fromUint(0)
        );

        const networkGenerationHash = this.networkGenerationHash;
        const signedAggTransaction = document.documentAccount.sign(aggTransaction, networkGenerationHash);
        return signedAggTransaction;
    }

    /**
     * Create VERIFY transaction to sign a document
     */
    createDocVerifyingTransaction(document: SiriusSignDocument, meta?: KeyValueObject) {
        const isOwner = document.info.owner.publicKey == this.userAccount.publicKey;
        const ssMessage = new SiriusSignDocumentVerifyingMessage(
            this.appID,
            document.fileHash,
            document.fileName,
            document.fileType,
            isOwner,
            document.info.uploadTxHash,
            document.info.encryptType,
            meta
        );

        const message = ssMessage.toPlainMessage();

        const docVerifyingTransaction = TransferTransaction.create(
            Deadline.create(),
            document.info.documentAccount.address,
            [],
            message,
            this.networkType,
            UInt64.fromUint(0)
        );

        const networkGenerationHash = this.networkGenerationHash;
        const signedDocVerifyingTransaction = this.userAccount.sign(docVerifyingTransaction, networkGenerationHash);

        return signedDocVerifyingTransaction;
    }
}