import { SiriusSignMessage, KeyValueObject } from "./SiriusSignMessage";
import { SiriusSignMessageType } from "./SiriusSignMessageType";
import { APPLICATION } from "../config/constant";

export class SiriusSignVerifierNotifyingMessage extends SiriusSignMessage {
    body: {
        uploadTxHash: string,
    };

    meta: KeyValueObject;

    constructor(
        appID: string,
        uploadTxHash: string,
        meta?: KeyValueObject
    ) {
        super(appID, SiriusSignMessageType.VERIFY_NOTIFY);
        this.body = {
            uploadTxHash: uploadTxHash,
        };
        this.meta = meta ? meta : {};
    }

    static createFromPayload(payload: string) {
        let message: SiriusSignVerifierNotifyingMessage;
        try {
            message = JSON.parse(payload);
        }
        catch (e) {
            throw new Error('M01: Payload is not a Sirius Sign message string');
        }
        if (message.header?.application != APPLICATION) throw new Error('M01: Payload is not a Sirius Sign message string');
        if (message.header?.messageType != SiriusSignMessageType.VERIFY) throw new Error('M05: This Sirius Sign Message is not a Verifier Notifying Message');

        return new SiriusSignVerifierNotifyingMessage(
            message.header.appID,
            message.body.uploadTxHash,
            message.meta
        );
    }
}
