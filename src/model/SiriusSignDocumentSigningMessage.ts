import { APPLICATION } from "../config/constant";
import { SiriusSignMessage, KeyValueObject } from "./SiriusSignMessage";
import { SiriusSignMessageType } from "./SiriusSignMessageType";
import { EncryptType } from "./EncryptType";

/**
 * Sirius Sign Document Signing Message that prove a document is signed by the transaction signer
 */
export class SiriusSignDocumentSigningMessage extends SiriusSignMessage {
    body: {
        fileHash: string,
        fileName: string,
        fileType: string,
        isOwner: boolean,
        uploadTxHash: string,
        encryptType: EncryptType,
        signatureUploadTxHash: string,
        // signaturePositions: SignaturePosition[],
    }

    meta: KeyValueObject;

    constructor(
        appID: string,
        fileHash: string,
        fileName: string,
        fileType: string,
        isOwner: boolean,
        uploadTxHash: string,
        encryptType: EncryptType,
        signatureUploadTxHash: string,
        // signaturePositions: SignaturePosition[], 
        meta?: KeyValueObject) {
        super(appID, SiriusSignMessageType.SIGN);
        this.body = {
            fileHash: fileHash,
            fileName: fileName,
            fileType: fileType,
            isOwner: isOwner,
            uploadTxHash: uploadTxHash,
            encryptType: encryptType,
            signatureUploadTxHash: signatureUploadTxHash,
            // signaturePositions: signaturePositions,
        }
        this.meta = meta ? meta : {};
    }

    static createFromPayload(payload: string) {
        let message: SiriusSignDocumentSigningMessage;
        try {
            message = JSON.parse(payload);
        }
        catch (e) {
            throw new Error('M01: Payload is not a Sirius Sign message string');
        }
        if (message.header?.application != APPLICATION) throw new Error('M01: Payload is not a Sirius Sign message string');
        if (message.header?.messageType != SiriusSignMessageType.SIGN) throw new Error('M02: This Sirius Sign Message is not a Document Signing Message');
        
        return new SiriusSignDocumentSigningMessage(
            message.header.appID,
            message.body.fileHash,
            message.body.fileName,
            message.body.fileType,
            message.body.isOwner,
            message.body.uploadTxHash,
            message.body.encryptType,
            message.body.signatureUploadTxHash,
            message.meta
        );
    }
}