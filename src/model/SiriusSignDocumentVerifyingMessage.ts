import { SiriusSignMessage, KeyValueObject } from "./SiriusSignMessage";
import { SiriusSignMessageType } from "./SiriusSignMessageType";
import { EncryptType } from "./EncryptType";
import { APPLICATION } from "../config/constant";

export class SiriusSignDocumentVerifyingMessage extends SiriusSignMessage {
    body: {
        fileHash: string,
        fileName: string,
        fileType: string,
        isOwner: boolean,
        uploadTxHash: string,
        encryptType: EncryptType,
    }

    meta: KeyValueObject;

    constructor(
        appID: string,
        fileHash: string,
        fileName: string,
        fileType: string,
        isOwner: boolean,
        uploadTxHash: string,
        encryptType: EncryptType,
        meta?: KeyValueObject
    ) {
        super(appID, SiriusSignMessageType.VERIFY);
        this.body = {
            fileHash: fileHash,
            fileName: fileName,
            fileType: fileType,
            isOwner: isOwner,
            uploadTxHash: uploadTxHash,
            encryptType: encryptType,
        };
        this.meta = meta ? meta : {};
    }

    static createFromPayload(payload: string) {
        let message: SiriusSignDocumentVerifyingMessage;
        try {
            message = JSON.parse(payload);
        }
        catch (e) {
            throw new Error('M01: Payload is not a Sirius Sign message string');
        }
        if (message.header?.application != APPLICATION) throw new Error('M01: Payload is not a Sirius Sign message string');
        if (message.header?.messageType != SiriusSignMessageType.VERIFY) throw new Error('M04: This Sirius Sign Message is not a Document Verifying Message');

        return new SiriusSignDocumentVerifyingMessage(
            message.header.appID,
            message.body.fileHash,
            message.body.fileName,
            message.body.fileType,
            message.body.isOwner,
            message.body.uploadTxHash,
            message.body.encryptType,
            message.meta
        );
    }
}