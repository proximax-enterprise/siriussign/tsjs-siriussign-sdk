import * as CryptoJS from 'crypto-js';
import { Account, PublicAccount } from 'tsjs-xpx-chain-sdk';
import { ConverterUtil } from '../util/ConverterUtil';
import { Util } from '../util/Util';
import { SiriusSignDocumentInfo, SignerInfo } from './SiriusSignDocumentInfo';
import { EncryptType } from './EncryptType';

export class SiriusSignDocument {
    documentAccount: Account;
    signatureUploadTxHash: string;
    completedDocumentData: Uint8Array;

    private constructor(
        public fileName: string,
        public fileType: string,
        public fileData: Uint8Array,
        public fileDataUri: string,
        public fileHash: string,
        public info: SiriusSignDocumentInfo
    ) { }

    public static createFromFile(fileName: string, fileType: string, fileData: Uint8Array) {
        const fileDataUri = 'data:' + fileType + ';base64,' + ConverterUtil.uint8ArrayToBase64(fileData);
        const fileHash = CryptoJS.SHA256(fileDataUri).toString(CryptoJS.enc.Hex).toUpperCase();
        const initInfo = SiriusSignDocumentInfo.new(fileName, fileType, fileHash);
        return new SiriusSignDocument(fileName, fileType, fileData, fileDataUri, fileHash, initInfo);
    }

    public static createFromInfo(info: SiriusSignDocumentInfo) {
        return new SiriusSignDocument(info.fileName, info.fileType, null, null, info.fileHash, info);
    }

    setOwner(publicAccount: PublicAccount) {
        this.info.owner = {
            publicKey: publicAccount.publicKey,
            address: publicAccount.address.plain(),
            isSigned: false,
            signDate: null,
            signTxHash: null,
            signatureUploadTxHash: null
        }
        return this;
    }

    setCosigners(cosigners: PublicAccount[]) {
        this.info.cosigners = cosigners.map(cosigner => {
            const info: SignerInfo = {
                publicKey: cosigner.publicKey,
                address: cosigner.address.plain(),
                isSigned: false,
                signDate: null,
                signTxHash: null,
                signatureUploadTxHash: null
            }
            return info;
        });
        return this;
    }

    setVerifiers(verifiers: PublicAccount[]) {
        this.info.verifiers = verifiers.map(verifier => {
            const info: SignerInfo = {
                publicKey: verifier.publicKey,
                address: verifier.address.plain(),
                isSigned: false,
                signDate: null,
                signTxHash: null,
                signatureUploadTxHash: null
            }
            return info;
        });
        return this;
    }

    setFinalDocumentData(fileData: Uint8Array) {
        this.completedDocumentData = fileData;
    }

    setDocumentAccount(account: Account) {
        this.documentAccount = account;
        this.info.documentAccount = account.publicAccount;
        this.info.id = account.publicKey;
        return this;
    }

    setEncryptType(encryptType: EncryptType) {
        this.info.encryptType = encryptType;
        return this;
    }

    setUploadTxHash(hash: string) {
        this.info.uploadTxHash = hash;
        return this;
    }

    setSignatureUploadTxhash(signer: PublicAccount, hash: string) {
        this.signatureUploadTxHash = hash;
        this.info.addSignatureUploadTxHash(signer.address.plain(), hash);
        return this;
    }

    setSignTxHash(signer: PublicAccount, hash: string, date: Date) {
        this.info.addSignTxHash(signer.address.plain(), hash, date);
        return this;
    }

    getDocumentAccountQrCode() {
        if (!this.info.documentAccount.publicKey) 
            throw new Error('D02: This document has not had an account yet');
        return Util.getQrCode(this.info.documentAccount.publicKey, 4);
    }
}