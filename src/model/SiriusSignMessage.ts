import { PlainMessage } from 'tsjs-xpx-chain-sdk';
import { SiriusSignMessageType } from "./SiriusSignMessageType";
import { SDK_VERSION, APPLICATION } from "../config/constant";

export interface KeyValueObject {
    [key: string]: any
}

export abstract class SiriusSignMessage {
    /**
     * Message Header to identify the transaction belongs to the app
     */
    header: {
        appID: string,
        application: string,
        version: string,
        messageType: SiriusSignMessageType
    };

    abstract body: KeyValueObject;

    abstract meta: KeyValueObject;

    constructor(appID: string, messageType: SiriusSignMessageType) {
        this.header = {
            appID: appID,
            application: APPLICATION,
            version: SDK_VERSION,
            messageType: messageType,
        }
    }

    /**
     * Create transaction Plain Message from Sirius Sign Message
     */
    toPlainMessage() {
        const message = {
            header: this.header,
            body: this.body,
            meta: this.meta
        };
        const messageString = JSON.stringify(message);
        return PlainMessage.create(messageString);
    }

    /**
     * Create transaction message string from Sirius Sign Message
     */
    toMessage() {
        const message = {
            header: this.header,
            body: this.body,
            meta: this.meta
        };
        return JSON.stringify(message);
    }

    public static isSiriusSignMessage(payload: string) {
        let message: SiriusSignMessage;
        try {
            message = JSON.parse(payload);
        }
        catch (e) {
            return false;
        }
        if (message.header?.application != APPLICATION) return false;
        return true;
    }

    public static getType(payload: string) {
        let message: SiriusSignMessage;
        try {
            message = JSON.parse(payload);
        }
        catch (e) {
            throw new Error('M01: Payload is not a Sirius Sign message string');
        }
        if (message.header?.application != APPLICATION) throw new Error('M01: Payload is not a Sirius Sign message string');
        if (!(message.header?.messageType ?? false)) throw new Error('M01: Payload is not a Sirius Sign message string');
        return message.header.messageType;
    }
}