export enum EncryptType {
    PLAIN,
    KEYPAIR,
    PASSWORD
}