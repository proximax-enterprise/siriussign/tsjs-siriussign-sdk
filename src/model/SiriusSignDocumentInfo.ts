import { PublicAccount, add } from 'tsjs-xpx-chain-sdk';
import { EncryptType } from './EncryptType';

export interface SignerInfo {
    address: string,
    publicKey: string
    isSigned: boolean,
    signDate: Date,
    signTxHash: string,
    signatureUploadTxHash: string
}

/**
 * SiriusDocument is a model of signing document used in SiriusSign.
 * It contains file and its notarization transaction infomation.
 */
export class SiriusSignDocumentInfo {

    isCompleted: boolean;
    isVerified: boolean;

    private constructor(
        /** Document ID */
        public id: string,
        /** File name */
        public fileName: string,
        /** File type */
        public fileType: string,
        /** File hash */
        public fileHash: string,
        /** File upload and sign date */
        public uploadDate: Date,
        /** File owner address, who sign this document first */
        public owner: SignerInfo,
        /** Document cosigners */
        public cosigners: SignerInfo[],
        /** Document Account that file belongs to */
        public documentAccount: PublicAccount,
        /** Upload transaction hash */
        public uploadTxHash: string,
        /** File is encrypt */
        public encryptType: EncryptType,
        /** Document verifiers */
        public verifiers: SignerInfo[]
    ) { }

    /**
     * Create a object that contains Sirius Sign Document Info
     */
    public static create(
        id: string,
        fileName: string,
        fileType: string,
        fileHash: string,
        uploadDate: Date,
        owner: SignerInfo,
        cosigners: SignerInfo[],
        documentAccount: PublicAccount,
        uploadTxHash: string,
        encryptType: EncryptType,
        verifiers: SignerInfo[]
    ) {
        const doc = new SiriusSignDocumentInfo(
            id,
            fileName,
            fileType,
            fileHash,
            uploadDate,
            owner,
            cosigners,
            documentAccount,
            uploadTxHash,
            encryptType,
            verifiers
        );
        doc.checkIsCompleted();
        doc.checkIsVerified();
        return doc;
    }

    /**
     * Create a object that contains Sirius Sign Document Info
     */
    public static new(
        fileName: string,
        fileType: string,
        fileHash: string,
    ) {
        const initOwner: SignerInfo = {
            address: null,
            publicKey: null,
            isSigned: false,
            signDate: null,
            signTxHash: null,
            signatureUploadTxHash: null
        }
        const doc = new SiriusSignDocumentInfo(null, fileName, fileType, fileHash, null, initOwner, [], null, null, EncryptType.PLAIN, []);
        doc.checkIsCompleted();
        return doc;
    }

    getCosignerAddresses() {
        const cosignerAddresses = this.cosigners.map(cosigner => cosigner.address);
        return cosignerAddresses;
    }

    getSignedCosignerAddresses() {
        const signedCosignerAddresses = this.cosigners.filter(cosigner => cosigner.isSigned).map(cosigner => cosigner.address);
        return signedCosignerAddresses;
    }

    getVerifierAddresses() {
        const verifierAddresses = this.verifiers.map(verifier => verifier.address);
        return verifierAddresses;
    }

    getVerifiedVerifierAddresses() {
        const verifiedVerifierAddresses = this.verifiers.filter(verifier => verifier.isSigned).map(verifier => verifier.address);
        return verifiedVerifierAddresses;
    }

    checkIsCompleted() {
        const cosigners = this.getCosignerAddresses();
        const cosignatures = this.getSignedCosignerAddresses();
        this.isCompleted = (cosigners.length == cosignatures.length) && (this.owner?.isSigned ?? false);
        return this.isCompleted;
    }

    checkIsVerified() {
        const verifiers = this.getVerifierAddresses();
        const verifieds = this.getVerifiedVerifierAddresses();
        return verifiers.length == verifieds.length;
    }

    addSignatureUploadTxHash(address: string, hash: string) {
        const isOwner = this.owner.address == address;
        if (isOwner) {
            this.owner.signatureUploadTxHash = hash;
        }
        else {
            const cosigners = this.getCosignerAddresses();
            const selectedIndex = cosigners.indexOf(address);
            if (selectedIndex < 0) throw new Error('D01: The address is not a signer of this document');
            this.cosigners[selectedIndex].signatureUploadTxHash = hash;
        }
        return this;
    }

    addSignTxHash(address: string, hash: string, date: Date) {
        const isOwner = this.owner.address == address;
        if (isOwner) {
            this.owner.signTxHash = hash;
            this.owner.signDate = date;
            this.owner.isSigned = true;
        }
        else {
            const cosigners = this.getCosignerAddresses();
            const selectedIndex = cosigners.indexOf(address);
            if (selectedIndex < 0) throw new Error('D01: The address is not a signer of this document');
            this.cosigners[selectedIndex].signTxHash = hash;
            this.cosigners[selectedIndex].signDate = date;
            this.cosigners[selectedIndex].isSigned = true;
        }
        return this;
    }

    checkIsSigner(address: string) {
        const isSigner = [this.owner, ...this.cosigners].map(signer => signer.address).includes(address);
        return isSigner;
    }

    checkIsVerifier(address: string) {
        const isVerifier = this.verifiers.map(verifier => verifier.address).includes(address);
        return isVerifier;
    }
}
