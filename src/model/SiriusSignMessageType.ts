/**
 * Sirius Sign Message Type
 */
export enum SiriusSignMessageType {
    SIGN = 1,
    SIGN_NOTIFY = 2,
    VERIFY = 3,
    VERIFY_NOTIFY = 4
}