import { APPLICATION } from "../config/constant";
import { SiriusSignMessageType } from "./SiriusSignMessageType";
import { SiriusSignMessage, KeyValueObject } from "./SiriusSignMessage";


export class SiriusSignCosignerNotifyingMessage extends SiriusSignMessage {
    body: {
        uploadTxHash: string,
        // signaturePositions: SignaturePosition[];
    }

    meta: KeyValueObject;

    constructor(
        appID: string,
        uploadTxHash: string,
        meta?: KeyValueObject
    ) {
        super(appID, SiriusSignMessageType.SIGN_NOTIFY);
        this.body = {
            uploadTxHash: uploadTxHash,
        }
        this.meta = meta ? meta : {};
    }

    static createFromPayload(payload: string) {
        let message: SiriusSignCosignerNotifyingMessage;
        try {
            message = JSON.parse(payload);
        }
        catch (e) {
            throw new Error('M01: Payload is not a Sirius Sign message string');
        }
        if (message.header?.application != APPLICATION) throw new Error('M01: Payload is not a Sirius Sign message string');
        if (message.header?.messageType != SiriusSignMessageType.SIGN_NOTIFY) throw new Error('M03: This Sirius Sign Message is not a Cosigner Notifying Message');

        return new SiriusSignCosignerNotifyingMessage(
            message.header.appID,
            message.body.uploadTxHash,
            message.meta
        );
    }
}