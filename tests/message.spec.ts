import { expect, assert } from 'chai';
import 'mocha';

import { APP_ID } from './constant.spec';
import { SiriusSignDocumentSigningMessage } from '../src/model/SiriusSignDocumentSigningMessage';
import { SiriusSignCosignerNotifyingMessage } from '../src/model/SiriusSignCosignerNotifyingMessage';
import { SiriusSignDocumentVerifyingMessage } from '../src/model/SiriusSignDocumentVerifyingMessage';
import { SiriusSignVerifierNotifyingMessage } from '../src/model/SiriusSignVerifierNotifyingMessage';
import { PlainMessage, TransferTransaction, Deadline, Address, Account, NetworkType } from 'tsjs-xpx-chain-sdk';
import { EncryptType } from '../src/model/EncryptType';

describe('Create Sirius Sign Document Signing Message', () => {
    it('should create a Document Signing Message object', () => {
        const docSigningMsg = new SiriusSignDocumentSigningMessage(
            APP_ID,
            'TESTFILEHASH',
            'Test File Name.pdf',
            'application/pdf',
            true,
            'TESTUPLOADSTRINGHASH',
            EncryptType.PLAIN,
            'SIGNATUREUPLOADTXHASH',
            new Map<string, string>([
                ['signatureUploadTxHash', 'TESTSIGNATUREUPLOADTXHASH']
            ])
        );

        expect(docSigningMsg).to.be.a.instanceof(SiriusSignDocumentSigningMessage);
    });

    it('should create a Document Signing Message object evenif no meta input', () => {
        const docSigningMsg = new SiriusSignDocumentSigningMessage(
            APP_ID,
            'TESTFILEHASH',
            'Test File Name.pdf',
            'application/pdf',
            true,
            'TESTUPLOADSTRINGHASH',
            EncryptType.PLAIN,
            'SIGNATUREUPLOADTXHASH',
        );

        expect(docSigningMsg).to.be.a.instanceof(SiriusSignDocumentSigningMessage);
    });

    it('should throw an error if input payload is not a Sirius Sign message', () => {
        try {
            SiriusSignDocumentSigningMessage.createFromPayload('Any message payload');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }

        try {
            SiriusSignDocumentSigningMessage.createFromPayload('');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }

        try {
            SiriusSignDocumentSigningMessage.createFromPayload('{"header":{"application":"Another application"}}');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }
    });

    it('should throw an error if input payload is not a Document Signing Message', () => {
        try {
            SiriusSignDocumentSigningMessage.createFromPayload('{"header": {"appID":"AppName", "application":"SiriusSign", "version":"0.0.1", "messageType": "0"}, "body": {}, "meta": {}}');
            assert.fail('expected exception not thrown');
        }
        catch (e) {
            expect(e.message).to.equal('M02: This Sirius Sign Message is not a Document Signing Message');
        }
    });

    // it('should throw an error if input payload is a Document Signing Message but missing properties', () => {
    //     try {
    //         const a = SiriusSignDocumentSigningMessage.createFromPayload('{"header": {"appID":"AppName", "application":"SiriusSign", "version":"0.0.1", "messageType": "1"}, "body": {}, "meta": {}}')
    //         assert.fail('expected exception not thrown');
    //     }
    //     catch (e) {
    //         expect(e.message).to.equal('M02: This Sirius Sign Message is not a Document Signing Message');
    //     }
    // });
});

describe('Create Sirius Sign Cosigner Notifying Message', () => {
    it('should create a Document Signing Message object', () => {
        const docSigningMsg = new SiriusSignCosignerNotifyingMessage(
            APP_ID,
            'TESTCOSIGNERNOTIFYINGHASH',
            new Map<string, string>([
                ['signatureUploadTxHash', 'TESTSIGNATUREUPLOADTXHASH']
            ])
        );

        expect(docSigningMsg).to.be.a.instanceof(SiriusSignCosignerNotifyingMessage);
    });

    it('should throw an error if input payload is not a Sirius Sign message', () => {
        try {
            SiriusSignCosignerNotifyingMessage.createFromPayload('Any message payload');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }

        try {
            SiriusSignCosignerNotifyingMessage.createFromPayload('');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }

        try {
            SiriusSignCosignerNotifyingMessage.createFromPayload('{"header":{"application":"Another application"}}');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }
    });

    it('should throw an error if input payload is not a Document Signing Message', () => {
        try {
            SiriusSignCosignerNotifyingMessage.createFromPayload('{"header": {"appID":"AppName", "application":"SiriusSign", "version":"0.0.1", "messageType": "1"}, "body": {}, "meta": {}}');
            assert.fail('expected exception not thrown');
        }
        catch (e) {
            expect(e.message).to.equal('M03: This Sirius Sign Message is not a Cosigner Notifying Message');
        }
    });
});

describe('Create Sirius Sign Document Verifying Message', () => {
    it('should create a Document Signing Message object', () => {
        const docSigningMsg = new SiriusSignDocumentVerifyingMessage(
            APP_ID,
            'TESTFILEHASH',
            'Test File Name.pdf',
            'application/pdf',
            true,
            'TESTUPLOADSTRINGHASH',
            EncryptType.PLAIN,
            {
                signatureUploadTxHash: 'TESTSIGNATUREUPLOADTXHASH'
            }
        );

        expect(docSigningMsg).to.be.a.instanceof(SiriusSignDocumentVerifyingMessage);
    });

    it('should create a Document Signing Message object evenif no meta input', () => {
        const docSigningMsg = new SiriusSignDocumentVerifyingMessage(
            APP_ID,
            'TESTFILEHASH',
            'Test File Name.pdf',
            'application/pdf',
            true,
            'TESTUPLOADSTRINGHASH',
            EncryptType.PLAIN
        );

        expect(docSigningMsg).to.be.a.instanceof(SiriusSignDocumentVerifyingMessage);
    });

    it('should throw an error if input payload is not a Sirius Sign message', () => {
        try {
            SiriusSignDocumentVerifyingMessage.createFromPayload('Any message payload');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }

        try {
            SiriusSignDocumentVerifyingMessage.createFromPayload('');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }

        try {
            SiriusSignDocumentVerifyingMessage.createFromPayload('{"header":{"application":"Another application"}}');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }
    });

    it('should throw an error if input payload is not a Document Signing Message', () => {
        try {
            SiriusSignDocumentVerifyingMessage.createFromPayload('{"header": {"appID":"AppName", "application":"SiriusSign", "version":"0.0.1", "messageType": "0"}, "body": {}, "meta": {}}');
            assert.fail('expected exception not thrown');
        }
        catch (e) {
            expect(e.message).to.equal('M04: This Sirius Sign Message is not a Document Verifying Message');
        }
    });
});

describe('Create Sirius Sign Verifier Notifying Message', () => {
    it('should create a Document Signing Message object', () => {
        const docSigningMsg = new SiriusSignVerifierNotifyingMessage(
            APP_ID,
            'TESTCOSIGNERNOTIFYINGHASH',
            new Map<string, string>([
                ['signatureUploadTxHash', 'TESTSIGNATUREUPLOADTXHASH']
            ])
        );

        expect(docSigningMsg).to.be.a.instanceof(SiriusSignVerifierNotifyingMessage);
    });

    it('should throw an error if input payload is not a Sirius Sign message', () => {
        try {
            SiriusSignVerifierNotifyingMessage.createFromPayload('Any message payload');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }

        try {
            SiriusSignVerifierNotifyingMessage.createFromPayload('');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }

        try {
            SiriusSignVerifierNotifyingMessage.createFromPayload('{"header":{"application":"Another application"}}');
            assert.fail('expected exception not thrown');
        } catch (e) {
            expect(e.message).to.equal('M01: Payload is not a Sirius Sign message string');
        }
    });

    it('should throw an error if input payload is not a Document Signing Message', () => {
        try {
            SiriusSignVerifierNotifyingMessage.createFromPayload('{"header": {"appID":"AppName", "application":"SiriusSign", "version":"0.0.1", "messageType": "1"}, "body": {}, "meta": {}}');
            assert.fail('expected exception not thrown');
        }
        catch (e) {
            expect(e.message).to.equal('M05: This Sirius Sign Message is not a Verifier Notifying Message');
        }
    });
});

describe('Create Plain Message from Sirius Sign Message', () => {
    it('should create a PlainMessage contain Document Signing Message', () => {
        const docSigningMsg = new SiriusSignDocumentSigningMessage(
            APP_ID,
            'TESTFILEHASH',
            'Test File Name.pdf',
            'application/pdf',
            true,
            'TESTUPLOADSTRINGHASH',
            EncryptType.PLAIN,
            'SIGNATUREUPLOADTXHASH',
            {
                isMulti: true
            }
        );
        const plainMsg = docSigningMsg.toPlainMessage();

        expect(plainMsg).to.be.instanceof(PlainMessage);
    });

    it('should create a PlainMessage contain Cosigner Notifying Message', () => {
        const docSigningMsg = new SiriusSignCosignerNotifyingMessage(
            APP_ID,
            'TESTFILEHASH',
            {
                fileHash: 'TESTFILEHASH'
            }
        );
        const plainMsg = docSigningMsg.toPlainMessage();

        expect(plainMsg).to.be.instanceof(PlainMessage);
    });

    it('should create a PlainMessage contain Document Verifying Message', () => {
        const docSigningMsg = new SiriusSignDocumentVerifyingMessage(
            APP_ID,
            'TESTFILEHASH',
            'Test File Name.pdf',
            'application/pdf',
            true,
            'TESTUPLOADSTRINGHASH',
            EncryptType.PLAIN,
            {
                isMulti: true
            }
        );
        const plainMsg = docSigningMsg.toPlainMessage();

        expect(plainMsg).to.be.instanceof(PlainMessage);
    });

    it('should create a PlainMessage contain Verifier Notifying Message', () => {
        const docSigningMsg = new SiriusSignVerifierNotifyingMessage(
            APP_ID,
            'TESTFILEHASH',
            {
                fileHash: 'TESTFILEHASH',
            }
        );
        const plainMsg = docSigningMsg.toPlainMessage();

        expect(plainMsg).to.be.instanceof(PlainMessage);
    });
});