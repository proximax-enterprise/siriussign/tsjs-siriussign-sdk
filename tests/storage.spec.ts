import { expect } from 'chai';
// import chaiAsPromised from 'chai-as-promised';
import 'mocha';
// chai.use(chaiAsPromised);

import { Account } from 'tsjs-xpx-chain-sdk';
import { DownloadResult } from 'tsjs-chain-xipfs-sdk';
import { API_NODE, IPFS_NODE, NETWORK_TYPE, PRIVATE_KEY } from './constant.spec';
import { ConverterUtil } from '../src/util/ConverterUtil';
import { SiriusSignDocument } from '../src/model/SiriusSignDocument';
import { EncryptType } from '../src/model/EncryptType';
import { DocumentStorage } from '../src/service/DocumentStorage';

describe.skip('Upload/Download a Sirius Sign document', () => {
    it('should upload a plain file then download the same file', async () => {
        const docStorage = new DocumentStorage(API_NODE, IPFS_NODE, NETWORK_TYPE);
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);
        const ssDoc = SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
        const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
        const docAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_2, NETWORK_TYPE);
        ssDoc.setDocumentAccount(docAcc)
            .setEncryptType(EncryptType.PLAIN);
        const sleep = (ms: number) => {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        // Uplaod
        const uploadResult = await docStorage.uploadDocument(ssDoc, ownerAcc);
        await sleep(10000); // Waiting for transaction confirmed
        // Download
        const downloadResult: DownloadResult = await docStorage.downloadDocument(uploadResult.transactionHash, EncryptType.PLAIN);

        const downloadData = await downloadResult.data?.getContentsAsString('base64');
        const fileType = downloadResult.data?.contentType;
        const downloadedDataUri = 'data:' + fileType + ';base64,' + encodeURI(downloadData);

        expect(downloadedDataUri).to.equal(dataUri);
    }).timeout(20000);

    it('should upload a file then download the same file with NEMKeyPrivacy', async () => {
        const docStorage = new DocumentStorage(API_NODE, IPFS_NODE, NETWORK_TYPE);
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);
        const ssDoc = SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
        const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
        const docAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_2, NETWORK_TYPE);
        ssDoc.setDocumentAccount(docAcc)
            .setEncryptType(EncryptType.KEYPAIR);
        const sleep = (ms: number) => {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        // Uplaod
        const uploadResult = await docStorage.uploadDocument(ssDoc, ownerAcc);
        await sleep(10000); // Waiting for transaction confirmed
        // Download
        const downloadResult: DownloadResult = await docStorage.downloadDocument(uploadResult.transactionHash, EncryptType.KEYPAIR, ownerAcc);

        const downloadData = await downloadResult.data?.getContentsAsString('base64');
        const fileType = downloadResult.data?.contentType;
        const downloadedDataUri = 'data:' + fileType + ';base64,' + encodeURI(downloadData);

        expect(downloadedDataUri).to.equal(dataUri);
    }).timeout(20000);

    // it.only('should throw Error when upload a file then download the same file with wrong NEMKeyPrivacy', async () => {
    //     const func = async () => {
    //         const docStorage = new DocumentStorage(API_NODE, IPFS_NODE, NETWORK_TYPE);
    //         const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
    //         const data = ConverterUtil.dataURIToUint8Array(dataUri);
    //         const ssDoc = new SiriusSignDocument('test.txt', 'text/plain', data);
    //         const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
    //         const docAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_2, NETWORK_TYPE);
    //         const sleep = (ms: number) => {
    //             return new Promise(resolve => setTimeout(resolve, ms));
    //         }

    //         // Uplaod
    //         const uploadResult = await docStorage.uploadDocument(ssDoc, docAcc, ownerAcc, EncryptType.KEYPAIR);
    //         await sleep(10000); // Waiting for transaction confirmed
    //         // Download
    //         const downloadResult: DownloadResult = await docStorage.downloadDocument(uploadResult.transactionHash, EncryptType.KEYPAIR, docAcc);

    //         const downloadData = await downloadResult.data?.getContentsAsString('base64');
    //         const fileType = downloadResult.data?.contentType;
    //         const downloadedDataUri = 'data:' + fileType + ';base64,' + encodeURI(downloadData);
    //     }

    //     expect(async () => await func()).to.be.rejectedWith(Error);
    // }).timeout(20000);

    it.skip('should upload a file then download the same file with PasswordPrivacy', async () => {
        const docStorage = new DocumentStorage(API_NODE, IPFS_NODE, NETWORK_TYPE);
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);
        const ssDoc = SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
        const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
        const docAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_2, NETWORK_TYPE);
        const sleep = (ms: number) => {
            return new Promise(resolve => setTimeout(resolve, ms));
        }
        ssDoc.setDocumentAccount(docAcc)
            .setEncryptType(EncryptType.PASSWORD);
        const password = 'password123';

        // Uplaod
        const uploadResult = await docStorage.uploadDocument(ssDoc, ownerAcc, password);
        await sleep(10000); // Waiting for transaction confirmed
        // Download
        const downloadResult: DownloadResult = await docStorage.downloadDocument(uploadResult.transactionHash, EncryptType.PASSWORD, password);

        const downloadData = await downloadResult.data?.getContentsAsString('base64');
        const fileType = downloadResult.data?.contentType;
        const downloadedDataUri = 'data:' + fileType + ';base64,' + encodeURI(downloadData);

        expect(downloadedDataUri).to.equal(dataUri);
    }).timeout(20000);

    it.skip('should upload a plain file then download the same file using uploadCompleted', async () => {
        const docStorage = new DocumentStorage(API_NODE, IPFS_NODE, NETWORK_TYPE);
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);
        const ssDoc = SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
        const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
        const docAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_2, NETWORK_TYPE);
        const sleep = (ms: number) => {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        // Uplaod
        const uploadResult = await docStorage.uploadCompletedDoc(
            'test.txt',
            'text/plain',
            'FD8B11F530783EA9623C505AD9A870CE574DDB51C1F58252DF3482AAF7025A9B',
            data,
            ownerAcc,
            docAcc.publicAccount,
            EncryptType.PLAIN
        );
        await sleep(10000); // Waiting for transaction confirmed
        // Download
        const downloadResult: DownloadResult = await docStorage.downloadDocument(uploadResult.transactionHash, EncryptType.PLAIN);

        const downloadData = await downloadResult.data?.getContentsAsString('base64');
        const fileType = downloadResult.data?.contentType;
        const downloadedDataUri = 'data:' + fileType + ';base64,' + encodeURI(downloadData);

        expect(downloadedDataUri).to.equal(dataUri);
    }).timeout(20000);
});