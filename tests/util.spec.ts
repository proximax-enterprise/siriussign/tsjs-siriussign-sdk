import { expect, assert } from 'chai';
import 'mocha';

import { NodeUtil } from '../src/util/NodeUtil';
import { ConverterUtil } from '../src/util/ConverterUtil';
import { NetworkType } from 'tsjs-xpx-chain-sdk';

describe('Node Util Test', () => {
    it('should split node to host, protocol, undefined port', () => {
        const node = 'https://bctestnet1.brimstone.xpxsirius.io';
        const nodeInfo = NodeUtil.split(node);

        expect(nodeInfo.domain).to.be.equal('bctestnet1.brimstone.xpxsirius.io');
        expect(nodeInfo.protocol).to.be.equal('https');
        expect(nodeInfo.port).to.be.equal(undefined);
    });

    it('should split node to host, protocol, port', () => {
        const node = 'http://bctestnet1.brimstone.xpxsirius.io:3000';
        const nodeInfo = NodeUtil.split(node);

        expect(nodeInfo.domain).to.be.equal('bctestnet1.brimstone.xpxsirius.io');
        expect(nodeInfo.protocol).to.be.equal('http');
        expect(nodeInfo.port).to.be.equal(3000);
    });

    it('should return right network generation hash', async () => {
        const node = 'http://bctestnet1.brimstone.xpxsirius.io:3000';
        const generationHash = await NodeUtil.fetchNetworkGenerationHash(node);
        const manualGetGenerationHash = '56D112C98F7A7E34D1AEDC4BD01BC06CA2276DD546A93E36690B785E82439CA9';

        expect(generationHash).to.be.equal(manualGetGenerationHash);
    });

    it('should return right network type', async () => {
        const node = 'https://bctestnet1.brimstone.xpxsirius.io';
        const networkType = await NodeUtil.fetchNetworkType(node);

        expect(networkType).to.be.equal(NetworkType.TEST_NET);

        const node2 = 'https://demo-sc-api-1.ssi.xpxsirius.io';
        const networkType2 = await NodeUtil.fetchNetworkType(node2);

        expect(networkType2).to.be.equal(NetworkType.PRIVATE_TEST);
    });
});

describe('Converter Util Test', () => {
    it('should convert base64 string to hex string', () => {
        const base64Str = 'VGhlIHdlYnNpdGUgdXNlcyBjb29raWVzLCB3ZWIgYmVhY29ucywgSmF2YVNjcmlwdCBhbmQgc2ltaWxhciB0ZWNobm9sb2dpZXM=';
        const hexString = ConverterUtil.base64ToHex(base64Str);
        expect(hexString).to.equal('5468652077656273697465207573657320636f6f6b6965732c2077656220626561636f6e732c204a61766153637269707420616e642073696d696c617220746563686e6f6c6f67696573');
    });

    it('should convert base64 string to hex string then revert the original base64 string', () => {
        const base64Str = 'VGhlIHdlYnNpdGUgdXNlcyBjb29raWVzLCB3ZWIgYmVhY29ucywgSmF2YVNjcmlwdCBhbmQgc2ltaWxhciB0ZWNobm9sb2dpZXM=';
        const hexString = ConverterUtil.base64ToHex(base64Str);
        const revertedBase64Str = ConverterUtil.hexToBase64(hexString);

        expect(revertedBase64Str).to.equal(base64Str);
    });

    it('should convert dataUri base64 string to Uint8Array', () => {
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);

        expect(data).to.be.a.instanceof(Uint8Array);
    });

    it('should convert dataUri base64 string to Uint8Array then revert to the original dataUri', () => {
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);
        const revertedDataUri = 'data:text/plain;base64,' + ConverterUtil.uint8ArrayToBase64(data);

        expect(revertedDataUri).to.equal(dataUri);
    });

    it('should convert dataURI to uint8array then to base64 then revert original dataURI', () => {
        const fileType = 'text/plain';
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);
        const base64 = ConverterUtil.uint8ArrayToBase64(data);
        const revertedDataUri = ConverterUtil.base64ToDataURI(base64, fileType);

        expect(revertedDataUri).to.be.equal(dataUri);
    })
})