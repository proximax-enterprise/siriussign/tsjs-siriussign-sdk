import { NetworkType } from "tsjs-xpx-chain-sdk";

export const APP_ID = 'AppName';
export const API_NODE = 'https://demo-sc-api-1.ssi.xpxsirius.io';
export const IPFS_NODE = 'https://ipfs1-dev.xpxsirius.io:5443';
export const NETWORK_TYPE = NetworkType.PRIVATE_TEST;
export const PRIVATE_KEY = {
    privateTest_1: '7FBDAF941AFA4F24284B55128C65E5F99B727134A8C0E6F1ACD07DF81E16D11F',
    privateTest_2: 'CC7ADAB081658540A7FFB0EE0A8450EB8A80AB974D0BF70FFDC61E92072751A5'
}