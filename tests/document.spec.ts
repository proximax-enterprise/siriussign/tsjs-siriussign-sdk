import { expect, assert } from 'chai';
import 'mocha';

import { ConverterUtil } from '../src/util/ConverterUtil';
import { SiriusSignDocument } from '../src/model/SiriusSignDocument';

describe('Create Sirius Sign Document', () => {
    it('should create a right Document', () => {
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);
        const ssDoc = SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);

        expect(ssDoc).to.be.a.instanceof(SiriusSignDocument);
        expect(ssDoc.fileDataUri).to.equal(dataUri);
        expect(ssDoc.fileHash).to.equal('fd8b11f530783ea9623c505ad9a870ce574ddb51c1f58252df3482aaf7025a9b'.toUpperCase());
    });

    it('getCosignerAddresses should return empty with blank document', () => {
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);
        const ssDoc = SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
        const cosignerAddresses = ssDoc.info.getCosignerAddresses();
        expect(cosignerAddresses).to.be.eql([]);
    });

    it('getSignedCosignerAddresses should return empty with blank document', () => {
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);
        const ssDoc = SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
        const cosignatures = ssDoc.info.getSignedCosignerAddresses();
        expect(cosignatures).to.be.eql([]);
    });

    it('checkComplete should return false with blank document', () => {
        const dataUri = 'data:text/plain;base64,VGhpcyBpcyBhIHRleHQgZmlsZS4K';
        const data = ConverterUtil.dataURIToUint8Array(dataUri);
        const ssDoc = SiriusSignDocument.createFromFile('test.txt', 'text/plain', data);
        const isComplete = ssDoc.info.checkIsCompleted();
        expect(isComplete).to.be.equal(false);
    });
});
