import { expect, assert } from 'chai';
import 'mocha';
import { Account } from 'tsjs-xpx-chain-sdk';
import { PRIVATE_KEY, NETWORK_TYPE, APP_ID, API_NODE } from './constant.spec';
import { DocumentFetch } from '../src';

describe('Document fetch tests', () => {
    it('should get all completed, waiting docs info from owner', async () => {
        const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
        const documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE);
        const { completed, waiting, verified, verifying } = await documentFetch.allCompletedAndWaiting(ownerAcc.publicAccount);

        expect(completed.length).to.be.greaterThan(1);
        completed.forEach(doc => {
            expect(doc.isCompleted).to.be.equal(true);
            expect(doc.owner.address).to.be.equal(ownerAcc.address.plain());
            expect(doc.owner.publicKey).to.be.equal(ownerAcc.publicKey);
            expect(doc.owner.isSigned).to.be.equal(true);
        });

        expect(waiting.length).to.be.greaterThan(1);
        waiting.forEach(doc => {
            expect(doc.isCompleted).to.be.equal(false);
            expect(doc.owner.address).to.be.equal(ownerAcc.address.plain());
            expect(doc.owner.publicKey).to.be.equal(ownerAcc.publicKey);
            expect(doc.owner.isSigned).to.be.equal(true);
        });

        expect(verified.length).to.be.equal(0);
        expect(verifying.length).to.be.equal(0);
    }).timeout(90000);

    it('should get all completed, waiting docs info from co-signer', async () => {
        const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
        const cosignerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_2, NETWORK_TYPE);
        const documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE);
        const { completed, waiting, verified, verifying } = await documentFetch.allCompletedAndWaiting(cosignerAcc.publicAccount);

        expect(completed.length).to.be.greaterThan(1);
        completed.forEach(doc => {
            expect(doc.isCompleted).to.be.equal(true);
            expect(doc.owner.address).to.be.equal(ownerAcc.address.plain());
            expect(doc.owner.publicKey).to.be.equal(ownerAcc.publicKey);
            expect(doc.owner.isSigned).to.be.equal(true);

            expect(doc.cosigners[0].address).to.be.equal(cosignerAcc.address.plain());
            expect(doc.cosigners[0].publicKey).to.be.equal(cosignerAcc.publicKey);
            expect(doc.cosigners[0].isSigned).to.be.equal(true);
        });

        expect(waiting.length).to.be.equal(0);
        expect(verified.length).to.be.equal(0);
        expect(verifying.length).to.be.equal(0);
    }).timeout(60000);

    it('should get all needsign docs info from co-signer', async () => {
        const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
        const cosignerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_2, NETWORK_TYPE);
        const documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE);

        const { needSign, needVerify } = await documentFetch.allNeedSignVerify(cosignerAcc.publicAccount);

        expect(needSign.length).to.be.greaterThan(1);
        needSign.forEach(doc => {
            expect(doc.isCompleted).to.be.equal(false);
            expect(doc.owner.address).to.be.equal(ownerAcc.address.plain());
            expect(doc.owner.publicKey).to.be.equal(ownerAcc.publicKey);
            expect(doc.owner.isSigned).to.be.equal(true);

            expect(doc.cosigners[0].address).to.be.equal(cosignerAcc.address.plain());
            expect(doc.cosigners[0].publicKey).to.be.equal(cosignerAcc.publicKey);
            expect(doc.cosigners[0].isSigned).to.be.equal(false);
        });

        expect(needVerify.length).to.be.equal(0);
    }).timeout(60000);

    it('should get completed, waiting docs info from owner (by transaction page)', async () => {
        const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
        const documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE);

        const { completed, waiting, verified, verifying, nextHash } = await documentFetch.completedAndWaiting(ownerAcc.publicAccount, null);

        expect(completed.length).to.be.greaterThan(0);
        completed.forEach(doc => {
            expect(doc.isCompleted).to.be.equal(true);
            expect(doc.owner.address).to.be.equal(ownerAcc.address.plain());
            expect(doc.owner.publicKey).to.be.equal(ownerAcc.publicKey);
            expect(doc.owner.isSigned).to.be.equal(true);
        });

        expect(verified.length).to.be.equal(0);
        expect(verifying.length).to.be.equal(0);
    }).timeout(60000);

    it('should get all completed, waiting docs info from owner (by transaction page)', async () => {
        const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
        const documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE);

        let fetchRes = await documentFetch.completedAndWaiting(ownerAcc.publicAccount, null);
        let firstHash = null;
        let lastHash = fetchRes.nextHash;
        let completed = fetchRes.completed, waiting = fetchRes.waiting, verified = fetchRes.verified, verifying = fetchRes.verifying;
        while (firstHash != lastHash && lastHash != null) {
            fetchRes = await documentFetch.completedAndWaiting(ownerAcc.publicAccount, lastHash);
            completed = [...completed, ...fetchRes.completed];
            waiting = [...waiting, ...fetchRes.waiting];
            verified = [...verified, ...fetchRes.verified];
            verifying = [...verifying, ...fetchRes.verifying];
            firstHash = lastHash;
            lastHash = fetchRes.nextHash;
        }

        const allRes = await documentFetch.allCompletedAndWaiting(ownerAcc.publicAccount);

        allRes.completed.forEach(doc => {
            expect(completed.map(e => e.id).includes(doc.id)).to.be.equal(true);
        });

        allRes.waiting.forEach(doc => {
            expect(waiting.map(e => e.id).includes(doc.id)).to.be.equal(true);
        });

        allRes.verified.forEach(doc => {
            expect(verified.map(e => e.id).includes(doc.id)).to.be.equal(true);
        });

        allRes.verifying.forEach(doc => {
            expect(verifying.map(e => e.id).includes(doc.id)).to.be.equal(true);
        });

    }).timeout(120000);

    it('should get needsign docs info from co-signer (by transaction page)', async () => {
        const ownerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_1, NETWORK_TYPE);
        const cosignerAcc = Account.createFromPrivateKey(PRIVATE_KEY.privateTest_2, NETWORK_TYPE);
        const documentFetch = new DocumentFetch(APP_ID, API_NODE, NETWORK_TYPE, 99);

        const { needSign, needVerify, nextHash } = await documentFetch.needSignVerify(cosignerAcc.publicAccount, null);

        expect(needSign.length).to.be.greaterThan(1);
        needSign.forEach(doc => {
            expect(doc.isCompleted).to.be.equal(false);
            expect(doc.owner.address).to.be.equal(ownerAcc.address.plain());
            expect(doc.owner.publicKey).to.be.equal(ownerAcc.publicKey);
            expect(doc.owner.isSigned).to.be.equal(true);

            expect(doc.cosigners[0].address).to.be.equal(cosignerAcc.address.plain());
            expect(doc.cosigners[0].publicKey).to.be.equal(cosignerAcc.publicKey);
            expect(doc.cosigners[0].isSigned).to.be.equal(false);
        });

        expect(needVerify.length).to.be.equal(0);
    }).timeout(60000);
});