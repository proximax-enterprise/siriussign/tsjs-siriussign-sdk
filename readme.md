# tsjs-siriussign-sdk
SiriusSign SDK for Typescript and Javascript.  
A blockchain-based document notarization solution using Sirius chain. You can easily build your own document signing application, procedure and sign digital signature on your documents and keep them safe on ProximaX blockchain and storage.
## Build and install
In the sdk directory:  
```
npm install
```
```
npm run build
```
In your ts, js project:  
```
npm install file:///<path to sdk diretory>
```
## Troubleshooting
Some known issues:  
- Can't resolve base-table of multicodec in cids package  
- crypto package not work on browser (crypto.randomBytes is not a function)  

Fix: run ```node patch.js```