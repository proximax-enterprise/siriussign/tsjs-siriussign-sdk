const fs = require('fs');

function replaceFile(filePath, searchString, replaceString) {
    fs.readFile(filePath, 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        const result = data.replace(searchString, replaceString);

        fs.writeFile(filePath, result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
    });
}

f_crypto = 'node_modules/tsjs-xpx-chain-sdk/dist/src/core/crypto/Crypto.js';
replaceFile(f_crypto, 'require(\'crypto\');', 'require(\'crypto-browserify\');');

f_codec = 'node_modules/cids/src/index.js';
replaceFile(f_codec, 'require(\'multicodec/src/base-table\')', 'require(\'multicodec/src/base-table.json\')');